package com.sport.grsport.Part5;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sport.grsport.Adapters.TournamentsRowItems;
import com.sport.grsport.HelperClasses.Help;
import com.sport.grsport.HelperClasses.SharedPreference;
import com.sport.grsport.HelperClasses.TournamentDetailsClass;
import com.sport.grsport.Part2.EnterInformation;
import com.sport.grsport.Part3.Home;
import com.sport.grsport.Part4.CreateUserProfile;
import com.sport.grsport.Part4.Profile;
import com.sport.grsport.R;
import com.sport.grsport.part1.login;
import com.sport.grsport.part1.userInformationClass;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import static com.sport.grsport.HelperClasses.Help.firebase;
import static com.sport.grsport.HelperClasses.Help.isNetworkStatusAvialable;

public class ActiveTournament extends AppCompatActivity {

    TextView textView_from,textView_to,textView_age,textView_entryfees,textView_prize_first,textView_prize_second,textView_prize_third,textView_prize_fourth,textView_uniqueid,textView_startday,textView_endday,textView_tournament_name;
    String tournamentuniqueid,username,email,name,city,mobilenumber,type,club,tournamentname;
    SharedPreference sharedPreference;
    TournamentsRowItems post;
    Button button;
   // ArrayList<String> marrayList=null;
    Firebase usersRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_tournament);
        Firebase.setAndroidContext(this);

        post=new TournamentsRowItems();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(getApplicationContext(),login.class));
            finish();
        }
        sharedPreference=new SharedPreference(getApplicationContext());
        SETTEXT();

        getDATA();
        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }





    }

    private void SETTEXT(){
        textView_from=(TextView)findViewById(R.id.active_tournament_from);
        textView_to=(TextView)findViewById(R.id.active_tournament_to);
        textView_age=(TextView)findViewById(R.id.active_tournament_age_group);
        textView_entryfees=(TextView)findViewById(R.id.active_tournament_entryfees);
        textView_tournament_name=(TextView) findViewById(R.id.active_tournament_tournament_name);

        button=(Button)findViewById(R.id.buttonregisted);
        Bundle bundle=getIntent().getExtras();
        if(bundle==null)
            return;
        tournamentuniqueid=bundle.getString("uniqueid");

        textView_prize_first=(TextView)findViewById(R.id.active_tournament_cashprizefirst);
        textView_prize_second=(TextView)findViewById(R.id.active_tournament_cashprizesecond);
        textView_prize_third=(TextView)findViewById(R.id.active_tournament_cashprizethird);
        textView_prize_fourth=(TextView)findViewById(R.id.active_tournament_cashprizefourth);
        textView_uniqueid=(TextView)findViewById(R.id.active_tournament_uniqueid);
        textView_startday=(TextView)findViewById(R.id.active_tournament_startday);
        textView_endday=(TextView)findViewById(R.id.active_tournament_endday);
        HashMap<String,String> user=sharedPreference.getUserDetails();
        username=user.get(sharedPreference.KEY_USERNAME);
        email=user.get(sharedPreference.KEY_EMAIL);
        name=user.get(sharedPreference.KEY_NAME);
        city=user.get(sharedPreference.KEY_CITY);
        mobilenumber=user.get(sharedPreference.KEY_PHONE);
        type=user.get(sharedPreference.KEY_TYPE);
        club=user.get(sharedPreference.KEY_SchoolInformation);



    }


    private void getDATA(){
     final ProgressDialog   mprogress=new ProgressDialog(ActiveTournament.this);
        mprogress.setMessage("Loading..");
        mprogress.setIndeterminate(false);
        mprogress.setCancelable(false);
        mprogress.show();

        firebase.child("tournaments").child(tournamentuniqueid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               // for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                  //   post = snapshot.getValue(TournamentsRowItems.class);

                  post.setTournamentStatus((String) dataSnapshot.child("TournamentStatus").getValue());
               post.setIsTournamentRegistrationClosed((String) dataSnapshot.child("isTournamentRegistrationClosed").getValue());
               post.setTournamentAgeGroup((String) dataSnapshot.child("tournamentAgeGroup").getValue());
               post.setTournamentCashPrizeFirst((String) dataSnapshot.child("tournamentCashPrizeFirst").getValue());
                post.setTournamentCashPrizeSecond((String) dataSnapshot.child("tournamentCashPrizeSecond").getValue());
                post.setTournamentCashPrizeThird((String) dataSnapshot.child("tournamentCashPrizeThird").getValue());
                post.setTournamentCashPrizeFourth((String) dataSnapshot.child("tournamentCashPrizeFourth").getValue());
                post.setTournamentEndWeekday((String) dataSnapshot.child("tournamentEndWeekday").getValue());

                post.setTournamentEntryFees((String) dataSnapshot.child("tournamentEntryFees").getValue());
                post.setTournamentFrom((String) dataSnapshot.child("tournamentFrom").getValue());
                post.setTournamentISOVER((String) dataSnapshot.child("tournamentISOVER").getValue());
                post.setTournamentName((String) dataSnapshot.child("tournamentName").getValue());
                post.setTournamentStartWeekday((String) dataSnapshot.child("tournamentStartWeekday").getValue());
                post.setTournamentStatus((String) dataSnapshot.child("tournamentStatus").getValue());
                post.setTournamentTo((String) dataSnapshot.child("tournamentTo").getValue());
                post.setTournamentUniqueCode((String) dataSnapshot.child("tournamentUniqueCode").getValue());

                mprogress.dismiss();
                    SETDATA(post);

                if(type.equals("Single Registration")){
                    button.setVisibility(View.GONE);

                }else if(type.equals("Team Registration")){
                    checkuserexistornot(post);


                }
               // }
            }
            @Override public void onCancelled(FirebaseError error) {
                mprogress.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed () {
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }

    public void onclickactiveTournamentprevious(View view){
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }
    private void SETDATA(TournamentDetailsClass post){

        textView_from.setText(post.getTournamentFrom());
        textView_to.setText(post.getTournamentTo());
        textView_age.setText(post.getTournamentAgeGroup());
        textView_entryfees.setText(post.getTournamentEntryFees());
        textView_prize_first.setText("1st : "+post.getTournamentCashPrizeFirst());
        textView_prize_second.setText("2nd : "+post.getTournamentCashPrizeSecond());
        textView_prize_third.setText("3rd : "+post.getTournamentCashPrizeThird());
        textView_prize_fourth.setText("4th : "+post.getTournamentCashPrizeFourth());
        textView_uniqueid.setText(post.getTournamentUniqueCode());
        textView_endday.setText(post.getTournamentEndWeekday());
        textView_startday.setText(post.getTournamentStartWeekday());
        textView_tournament_name.setText(post.getTournamentName());

    }


    public void countprofile(){

        firebase.child("userdetails").child(username).orderByChild("checkparent").equalTo("yes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {



                    int noofchildpresent = (int) dataSnapshot.getChildrenCount();
                      if (noofchildpresent < 16) {

                          button.setVisibility(View.GONE);
                          final Dialog dialog=new Dialog(ActiveTournament.this);

                          dialog.setContentView(R.layout.dialoggoalcompleted);
                          dialog.setCancelable(false);

                          dialog.show();


                          TextView textView=(TextView) dialog.findViewById(R.id.dialogcompletetextview);
                          textView.setText("Please add atleast 16 members in your team to registered for this tournament.");
                          dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

                          TextView imageView1=(TextView) dialog.findViewById(R.id.dialogcompletetextview2);
                          imageView1.setOnClickListener(new View.OnClickListener() {
                              @Override
                              public void onClick(View view) {
                                  dialog.dismiss();
                              }
                          });

                    }else {
                          button.setVisibility(View.VISIBLE);
                      }
                }catch (Exception e)
                {

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        }) ;
    }


  /*  public void Registeredeventarraylist(){

        marrayList=new ArrayList<String>();
        firebase.child("userdetails").child(username).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                 marrayList = (ArrayList<String>) dataSnapshot.child("arraylisttournament").getValue();

                    Toast.makeText(getApplicationContext(),marrayList.size()+"",Toast.LENGTH_SHORT).show();

                }catch (Exception e)
                {

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        }) ;
    }*/

    public void checkuserexistornot(TournamentDetailsClass post){
         firebase.child("registeredtournament/tournament").child(post.getTournamentUniqueCode()).child(username).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String   checkname1=(String) dataSnapshot.child("name").getValue();

                if(dataSnapshot.getValue()!=null){

                    button.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(),"Already Registered !!",Toast.LENGTH_LONG).show();
                }else {
                  countprofile();

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


    public void OnclickRegisterTournament(View view) {

        if (isNetworkStatusAvialable(getApplicationContext())) {
        DateFormat df = new SimpleDateFormat("d MMM yyyy, HH:mm a");
        String date = df.format(Calendar.getInstance().getTime());

        userInformationClass userInformationClass = new userInformationClass();
        userInformationClass.setName(name);
        userInformationClass.setUsername(username);
        userInformationClass.setCity(city);
        userInformationClass.setEmail(email);
        userInformationClass.setContactnumber(mobilenumber);
        userInformationClass.setDateandTime(date);
            userInformationClass.setSchoolinstitudeinformation(club);
            userInformationClass.setRegistration("true");


        // marrayList.add("hi");

        final ProgressDialog mprogressDialog = new ProgressDialog(ActiveTournament.this);
        mprogressDialog.setCancelable(false);
        mprogressDialog.setTitle("loading...");
        mprogressDialog.show();
        firebase.child("registeredtournament/tournament").child(post.getTournamentUniqueCode()).child(username).setValue(userInformationClass, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase1) {
                if (firebaseError != null) {
                    mprogressDialog.dismiss();
                } else {
                    mprogressDialog.dismiss();
                    button.setVisibility(View.GONE);
                    final Dialog dialog = new Dialog(ActiveTournament.this);

                    dialog.setContentView(R.layout.dialoggoalcompleted);
                    dialog.setCancelable(false);

                    dialog.show();


                    TextView textView = (TextView) dialog.findViewById(R.id.dialogcompletetextview);
                    textView.setText("Registration Completed !!" + " \n " + " Your unique team id is " + email);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.black)));

                    TextView imageView1 = (TextView) dialog.findViewById(R.id.dialogcompletetextview2);
                    imageView1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        });

    } else {
            Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
        }
    }


    /*private class AsyncTaskRunner extends AsyncTask<String, Void, Void> {

        String check;


        @Override
        protected void doInBackground(String... params) {

            firebase.child("registeredtournament/tournament").child(post.getTournamentUniqueCode()).child(username).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String   checkname1=(String) dataSnapshot.child("name").getValue();

                    if(dataSnapshot.getValue()!=null){

                       check="true";
                    }else {
                        check="false";

                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });



        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation

           if(result.equals("true")){
               button.setVisibility(View.INVISIBLE);
           }else if ((result.equals("false"))){
               button.setVisibility(View.VISIBLE);
           }

        }


        @Override
        protected void onPreExecute() {

        }


        @Override
        protected void onProgressUpdate(String... text) {


        }
    }
*/


}
