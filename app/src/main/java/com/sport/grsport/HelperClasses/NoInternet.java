package com.sport.grsport.HelperClasses;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sport.grsport.Part3.Home;
import com.sport.grsport.R;

import static com.sport.grsport.HelperClasses.Help.isNetworkStatusAvialable;

public class NoInternet extends AppCompatActivity {
    TextView textView;
    RelativeLayout relativeLayouthidetryagain;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);


        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }
        if (isNetworkStatusAvialable(getApplicationContext())) {

            startActivity(new Intent(getApplicationContext(),Home.class));
            finish();
        }else {
            tryagainbutton();
        }


        //startActivity(new Intent(getApplicationContext(),findlocation.class));
        //    finish();
    }

    private void tryagainbutton(){
        relativeLayouthidetryagain=(RelativeLayout)findViewById(R.id.try_again_main_layout);
        relativeLayouthidetryagain.setVisibility(View.VISIBLE);
        ImageView imageView=(ImageView)findViewById(R.id.activity_main_imageview);
        imageView.setImageResource(R.drawable.front1);
        textView=(TextView) findViewById(R.id.try_again_main);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkStatusAvialable(getApplicationContext())) {
                    startActivity(new Intent(getApplicationContext(),Home.class));
                    finish();

                }else {
                    Toast.makeText(getApplicationContext(),"No internet Connection !!",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}











