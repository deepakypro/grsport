package com.sport.grsport.HelperClasses;

/**
 * Created by deepak on 29/01/17.
 */

public class TournamentDetailsClass {

    String tournamentFrom;

    String tournamentTo;
    String tournamentCashPrizeFirst;
    String tournamentCashPrizeSecond;
    String tournamentCashPrizeThird;
    String tournamentCashPrizeFourth;
    String tournamentAgeGroup;
    String tournamentEntryFees;

    public String getTournamentName() {
        return tournamentName;
    }

    public void setTournamentName(String tournamentName) {
        this.tournamentName = tournamentName;
    }

    String tournamentName;

    String tournamentISOVER;

    public String getTournamentISOVER() {
        return tournamentISOVER;
    }

    public void setTournamentISOVER(String tournamentISOVER) {
        this.tournamentISOVER = tournamentISOVER;
    }

    String tournamentStartWeekday;

    public String getTournamentEndWeekday() {
        return tournamentEndWeekday;
    }

    public void setTournamentEndWeekday(String tournamentEndWeekday) {
        this.tournamentEndWeekday = tournamentEndWeekday;
    }

    public String getTournamentStartWeekday() {
        return tournamentStartWeekday;
    }

    public void setTournamentStartWeekday(String tournamentStartWeekday) {
        this.tournamentStartWeekday = tournamentStartWeekday;
    }

    String tournamentEndWeekday;

    public String getTournamentUniqueCode() {
        return tournamentUniqueCode;
    }

    public void setTournamentUniqueCode(String tournamentUniqueCode) {
        this.tournamentUniqueCode = tournamentUniqueCode;
    }

    public String getTournamentFrom() {
        return tournamentFrom;
    }

    public void setTournamentFrom(String tournamentFrom) {
        this.tournamentFrom = tournamentFrom;
    }

    public String getTournamentTo() {
        return tournamentTo;
    }

    public void setTournamentTo(String tournamentTo) {
        this.tournamentTo = tournamentTo;
    }

    public String getTournamentCashPrizeFirst() {
        return tournamentCashPrizeFirst;
    }

    public void setTournamentCashPrizeFirst(String tournamentCashPrizeFirst) {
        this.tournamentCashPrizeFirst = tournamentCashPrizeFirst;
    }

    public String getTournamentCashPrizeSecond() {
        return tournamentCashPrizeSecond;
    }

    public void setTournamentCashPrizeSecond(String tournamentCashPrizeSecond) {
        this.tournamentCashPrizeSecond = tournamentCashPrizeSecond;
    }

    public String getTournamentCashPrizeThird() {
        return tournamentCashPrizeThird;
    }

    public void setTournamentCashPrizeThird(String tournamentCashPrizeThird) {
        this.tournamentCashPrizeThird = tournamentCashPrizeThird;
    }

    public String getTournamentCashPrizeFourth() {
        return tournamentCashPrizeFourth;
    }

    public void setTournamentCashPrizeFourth(String tournamentCashPrizeFourth) {
        this.tournamentCashPrizeFourth = tournamentCashPrizeFourth;
    }

    public String getTournamentAgeGroup() {
        return tournamentAgeGroup;
    }

    public void setTournamentAgeGroup(String tournamentAgeGroup) {
        this.tournamentAgeGroup = tournamentAgeGroup;
    }

    public String getTournamentEntryFees() {
        return tournamentEntryFees;
    }

    public void setTournamentEntryFees(String tournamentEntryFees) {
        this.tournamentEntryFees = tournamentEntryFees;
    }

   public String tournamentUniqueCode;

}
