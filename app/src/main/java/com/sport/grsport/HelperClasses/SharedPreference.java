package com.sport.grsport.HelperClasses;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.sport.grsport.Part2.EnterInformation;
import com.sport.grsport.part1.login;

import java.util.HashMap;

/**
 * Created by deepak on 26/01/17.
 */

public class SharedPreference  extends Application {
    // Shared Preferences
    SharedPreferences pref;
    //public boolean log=false;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "detail";

    // All Shared Preferences Keys
    private static final String IS_DETAILS = "IsDetailsIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";
    public static final String KEY_AGE = "age";
    public static final String KEY_PHONE = "phone";

    public static final String KEY_CITY = "City";

    public static final String KEY_USERNAME = "username";
    public static final String KEY_SchoolInformation="schoolinformation";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_TYPE = "type";


    // Constructor
    public SharedPreference(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createDetailsSession(String username,String name, String age,String phone, String city,String email,String schoolinformation,String type){
        // Storing login value as TRUE
        editor.putBoolean(IS_DETAILS, true);

        editor.putString(KEY_USERNAME, username);
        // Storing name in pref
        editor.putString(KEY_NAME, name);

        // Storing email in pref
        editor.putString(KEY_AGE, age);

        // Storing name in pref


        // Storing email in pref
        editor.putString(KEY_PHONE, phone);

        // Storing name in pref
        editor.putString(KEY_CITY, city);

        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_SchoolInformation, schoolinformation);
        editor.putString(KEY_TYPE, type);

        // Storing email in pref



        //editor.putString(KEY_GENDER, gender);
        // commit changes
        editor.commit();

    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public boolean checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity

            return false;

        }

        return true;
    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));

        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));


        user.put(KEY_AGE, pref.getString(KEY_AGE, null));


        user.put(KEY_PHONE, pref.getString(KEY_PHONE, null));



        user.put(KEY_CITY, pref.getString(KEY_CITY, null));

        //user.put(KEY_GENDER, pref.getString(KEY_GENDER, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_SchoolInformation, pref.getString(KEY_SchoolInformation, null));
        user.put(KEY_TYPE, pref.getString(KEY_TYPE, null));

        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
       // Intent i = new Intent(_context, login.class);
        // Closing all the Activities
        //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
      //  i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        //_context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_DETAILS, false);
    }
}



