package com.sport.grsport;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.sport.grsport.Adapters.HistoryRowItems;
import com.sport.grsport.Adapters.TournamentsRowItems;
import com.sport.grsport.HelperClasses.ImageUploadClass;
import com.sport.grsport.HelperClasses.NoInternet;
import com.sport.grsport.HelperClasses.TournamentDetailsClass;
import com.sport.grsport.Part2.EnterInformation;
import com.sport.grsport.Part3.Home;
import com.sport.grsport.Part3.ShowGallery;
import com.sport.grsport.Part4.MyCreateTeam;
import com.sport.grsport.part1.login;
import com.sport.grsport.part1.userInformationClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.sport.grsport.HelperClasses.Help.firebase;
import static com.sport.grsport.HelperClasses.Help.isNetworkStatusAvialable;

public class MainActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;

    FirebaseAuth mAuth;
    FirebaseAuth.AuthStateListener mAuthListner;
    FirebaseStorage storage;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MultiDex.install(this);

        storage=FirebaseStorage.getInstance();
        Firebase.setAndroidContext(this);
        mAuth=FirebaseAuth.getInstance();


       // SETTEXT();


        mAuthListner=new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {
                new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                    @Override
                    public void run() {

                        if (isNetworkStatusAvialable(getApplicationContext())) {
                            FirebaseUser user = firebaseAuth.getCurrentUser();
                            if (user != null) {
                                // User is signed in

                                startActivity(new Intent(getApplicationContext(), Home.class));
                                finish();
                            } else {
                                // No user is signed in

                                startActivity(new Intent(getApplicationContext(), login.class));
                                finish();
                            }
                        }else {
                            startActivity(new Intent(getApplicationContext(), NoInternet.class));
                            finish();
                        }




                    }
                }, SPLASH_TIME_OUT);
            }
        };
        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }






    }

    @Override
    public void onStart(){
        super.onStart();
        mAuth.addAuthStateListener(mAuthListner);
    }

    @Override
    public void onStop(){
        super.onStop();
        mAuth.removeAuthStateListener(mAuthListner);
    }


    private void setSponsor(){
        Calendar c=Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String workoutstarttime = df.format(c.getTime());
        ImageUploadClass imageUploadClass=new ImageUploadClass();
       // imageUploadClass.setImage("https://firebasestorage.googleapis.com/v0/b/grsport-69208.appspot.com/o/Tournament%2FMajor%20Dhyan%20Chand%20National%20Stadium.jpg?alt=media&token=3199e3c9-cf75-490c-acef-3fc84e78a563");
        imageUploadClass.setName("Jtee Sports");
        imageUploadClass.setValid("yes");
        imageUploadClass.setDate(workoutstarttime);
        firebase.child("Sponsor/SponsorGallery").push().setValue(imageUploadClass, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {

            }
        });
    }

    private void setSponsor2(){
        Calendar c=Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String workoutstarttime = df.format(c.getTime());
        ImageUploadClass imageUploadClass=new ImageUploadClass();
       // imageUploadClass.setImage("https://firebasestorage.googleapis.com/v0/b/grsport-69208.appspot.com/o/Tournament%2FMajor%20Dhyan%20Chand%20National%20Stadium.jpg?alt=media&token=3199e3c9-cf75-490c-acef-3fc84e78a563");
        imageUploadClass.setName("SNS Hockey");
        imageUploadClass.setValid("yes");
        imageUploadClass.setDate(workoutstarttime);
        firebase.child("Sponsor/SponsorGallery").push().setValue(imageUploadClass, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {

            }
        });
    }

    private void setSponsor3(){
        Calendar c=Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String workoutstarttime = df.format(c.getTime());
        ImageUploadClass imageUploadClass=new ImageUploadClass();
      //  imageUploadClass.setImage("https://firebasestorage.googleapis.com/v0/b/grsport-69208.appspot.com/o/Tournament%2FMajor%20Dhyan%20Chand%20National%20Stadium.jpg?alt=media&token=3199e3c9-cf75-490c-acef-3fc84e78a563");
        imageUploadClass.setName("Flash Hockey");
        imageUploadClass.setValid("yes");
        imageUploadClass.setDate(workoutstarttime);
        firebase.child("Sponsor/SponsorGallery").push().setValue(imageUploadClass, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {

            }
        });
    }

}
