package com.sport.grsport.Part2;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sport.grsport.Adapters.HistoryCustomAdapter;
import com.sport.grsport.Adapters.HistoryRowItems;
import com.sport.grsport.Part3.History;
import com.sport.grsport.Part3.Home;
import com.sport.grsport.R;

import java.util.ArrayList;
import java.util.List;

import static com.sport.grsport.HelperClasses.Help.firebase;

public class ContactUs extends AppCompatActivity {


    List<HistoryRowItems> rowItems;
    FirebaseStorage storage;
    ListView listView;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        rowItems = new ArrayList<>();
        Firebase.setAndroidContext(this);
        storage = FirebaseStorage.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        FetchImage();

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }

        Button button=(Button) findViewById(R.id.contactusshowdirection);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=28.531715,76.916404"));
                startActivity(intent);
            }
        });

    }

    public void onclickhistorypreviousactivity(View view) {
        startActivity(new Intent(getApplicationContext(), Home.class));
        finish();
    }

    private void FetchImage() {


        final ProgressDialog progressDialog = new ProgressDialog(ContactUs.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        firebase.child("Contactus").child("members").orderByChild("present").equalTo("yes").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String name = (String) dataSnapshot.child("name").getValue();
                String contact = (String) dataSnapshot.child("contactus").getValue();
                String image = (String) dataSnapshot.child("image").getValue();
                String post = (String) dataSnapshot.child("post").getValue();

                HistoryRowItems item = new HistoryRowItems();

                item.setName(name);
                item.setContactus(contact);

                item.setPost(post);
                item.setHistoryimagelink(image);

                progressDialog.dismiss();
                rowItems.add(item);

                if (rowItems.size() > 0) {
                    listView = (ListView) findViewById(R.id.contactuslistview);
                    HistoryCustomAdapter adapter = new HistoryCustomAdapter(getApplicationContext(), rowItems);
                    listView.setAdapter(adapter);
                    //listView.setOnItemClickListener(History.this);

                    ListUtils.setDynamicHeight(listView);
                    // Log.d("hello",value+"hello"+item.getImagelink());

                } else {

                    progressDialog.dismiss();
                    // Log.d("hello","hello");
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }


    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }

    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }

    public void onclickshowgalleryprevious(View view) {
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }
}