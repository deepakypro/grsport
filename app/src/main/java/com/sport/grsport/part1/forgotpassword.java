package com.sport.grsport.part1;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.sport.grsport.R;


public class forgotpassword extends AppCompatActivity {
    ProgressDialog mprogress;
    private EditText inputEmail;
    private Button btnReset, btnBack;
    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpassword);
        inputEmail = (EditText) findViewById(R.id.emailresetpassword);



        auth = FirebaseAuth.getInstance();
    }

    public void btn_back(View view){
        startActivity(new Intent(this,signUp.class));
        finish();
    }
    public void btn_reset_password(View view){
        String email = inputEmail.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplication(), "Enter your registered email id", Toast.LENGTH_SHORT).show();
            return;
        }

        mprogress=new ProgressDialog(forgotpassword.this);
        mprogress.setMessage("Loading..");
        mprogress.setIndeterminate(false);
        mprogress.show();
        auth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            mprogress.dismiss();
                            Toast.makeText(forgotpassword.this, "Email sent ", Toast.LENGTH_SHORT).show();
                        } else {
                            mprogress.dismiss();
                            Toast.makeText(forgotpassword.this, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
                        }


                    }
                });
    }
}

