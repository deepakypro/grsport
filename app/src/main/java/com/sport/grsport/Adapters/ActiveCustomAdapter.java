package com.sport.grsport.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sport.grsport.HelperClasses.CircleTransform;
import com.sport.grsport.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by deepak on 24/03/17.
 */

public class ActiveCustomAdapter extends BaseAdapter {
    private Context context;
    private List<TournamentsRowItems> rowItems;


    public   ActiveCustomAdapter(Context context, List<TournamentsRowItems> items) {
        this.context = context;
        this.rowItems = items;
    }

    private class ViewHolder {
        TextView name, date,status;

        // ImageView image1;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        // ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.lasyout_listview_active_tournament, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.listview_active_name);
            holder.date = (TextView) convertView.findViewById(R.id.listview_active_startdate);
            holder.status = (TextView) convertView.findViewById(R.id.listview_active_status);


            /// holder.distance = (TextView) convertView.findViewById(R.id.customlistworkoutdistance);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        //holder.date.setText(detail.getWorkoutdate());
        TournamentsRowItems detail = rowItems.get(position);
        // if(!date1.equals(detail.getWorkoutdate()))
        //{
        // detail = rowItems.get(position);
        //  date1="december2010";
        //date1=detail.getWorkoutdate();
        //countnumber=1;
        //holder.time.setText(detail.getWorkouttime());
        // holder.workoutname.setText(detail.getWorkouttime());

        holder.name.setText(detail.getTournamentName());
        // holder.workoutnumber.setText(countnumber+"");
        //holder.workoutname.setText(detail.getWorkouttime());

        convertView.setTag(holder);
        //}else {
        // detail = rowItems.get(position);
        // holder.time.setText(detail.getWorkouttime());
        holder.date.setText(detail.getTournamentFrom());
        holder.status.setText(detail.getTournamentStatus());

        /*Picasso.with(context).load(detail.getImagelink()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                RoundedBitmapDrawable drawable = Help.createRoundedBitmapDrawableWithBorder(bitmap);


                holder.imageView.setImageDrawable(drawable);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });*/


        //countnumber+=1;
        //holder.workoutnumber.setText(countnumber+"");
        //convertView.setTag(holder);

        //  }



        // holder.duration.setText(function.getDuration(detail.getDuration()));
        // Bitmap albumArt = function.getAlbumart(context, detail.getAlbumId());
        // holder.image1.setBackgroundDrawable(new BitmapDrawable(albumArt));
        // holder.image1.setImageResource(new BitmapDrawable(albumArt));
        return convertView;
    }


    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}
