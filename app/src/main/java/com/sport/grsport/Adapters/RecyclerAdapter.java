package com.sport.grsport.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sport.grsport.HelperClasses.CircleTransform;
import com.sport.grsport.HelperClasses.ImageLoader;
import com.sport.grsport.Part3.ShowGallery;
import com.sport.grsport.Part4.Profile;
import com.sport.grsport.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by deepak on 28/05/17.
 */

public class RecyclerAdapter  extends BaseAdapter {
    private Context context;
    private List<ProfileRowItems> rowItems;


    public   RecyclerAdapter(Context context, List<ProfileRowItems> items) {
        this.context = context;
        this.rowItems = items;
    }

    private class ViewHolder {
        TextView name;


        // ImageView image1;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final RecyclerAdapter.ViewHolder holder;

        // ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.listview_sponsor, null);
            holder = new RecyclerAdapter.ViewHolder();
            //holder.name = (TextView) convertView.findViewById(R.id.showimage_textview123);
            // holder.age = (TextView) convertView.findViewById(R.id.listview_team_member_age);
            holder.name = (TextView) convertView.findViewById(R.id.listview_sponsor_name);

            /// holder.distance = (TextView) convertView.findViewById(R.id.customlistworkoutdistance);

            convertView.setTag(holder);
        } else {
            holder = (RecyclerAdapter.ViewHolder) convertView.getTag();
        }


        //holder.date.setText(detail.getWorkoutdate());
        final ProfileRowItems detail = rowItems.get(position);
        holder.name.setText(detail.getName());

        convertView.setTag(holder);

        return convertView;
    }


    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}














       /* extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

    private List<ProfileRowItems> horizontalList;

    private Context context;
    public ImageLoader imageLoader;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView textview;

        public MyViewHolder(View view) {
            super(view);

            textview=(TextView) view.findViewById(R.id.listview_sponsor_name);
        }
    }


    public RecyclerAdapter(Context context ,List<ProfileRowItems> horizontalList) {
        this.context=context;
        this.horizontalList = horizontalList;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public RecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_sponsor, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerAdapter.MyViewHolder holder, final int position) {
        final ProfileRowItems detail = horizontalList.get(position);

        //imageLoader.DisplayImage(detail.getImagelink(), holder.imageView);
        holder.textview.setText(detail.getName());



    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}*/





