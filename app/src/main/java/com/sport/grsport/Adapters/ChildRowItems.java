package com.sport.grsport.Adapters;

import com.sport.grsport.part1.userInformationClass;

/**
 * Created by deepak on 04/02/17.
 */

public class ChildRowItems extends ProfileRowItems {

    public  String childuser;

    public String getChilduser() {
        return childuser;
    }

    public void setChilduser(String childuser) {
        this.childuser = childuser;
    }

    @Override
    public String toString(){
        return  getChilduser();
    }
}
