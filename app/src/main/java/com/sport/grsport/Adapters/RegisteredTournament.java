package com.sport.grsport.Adapters;

import com.sport.grsport.HelperClasses.TournamentDetailsClass;
import com.sport.grsport.part1.userInformationClass;

/**
 * Created by deepak on 04/02/17.
 */

public class RegisteredTournament extends userInformationClass {

    public String getTournamentUniqueCode() {
        return tournamentUniqueCode;
    }

    public void setTournamentUniqueCode(String tournamentUniqueCode) {
        this.tournamentUniqueCode = tournamentUniqueCode;
    }

    public String tournamentUniqueCode;
    @Override
    public String toString(){
        return tournamentUniqueCode;
    }
}
