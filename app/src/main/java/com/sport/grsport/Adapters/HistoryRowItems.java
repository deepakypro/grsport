package com.sport.grsport.Adapters;

/**
 * Created by deepak on 13/02/17.
 */

public class HistoryRowItems {

    public String name;

    public String present;
    public String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public String post;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getContactus() {
        return contactus;
    }

    public void setContactus(String contactus) {
        this.contactus = contactus;
    }

    public String contactus;

    public String historydate;
    public String historyeventname;
    public String historyimagelink;
    public String historydescription;

    public String getHistorydate() {
        return historydate;
    }

    public void setHistorydate(String historydate) {
        this.historydate = historydate;
    }

    public String getHistoryeventname() {
        return historyeventname;
    }

    public void setHistoryeventname(String historyeventname) {
        this.historyeventname = historyeventname;
    }

    public String getHistoryimagelink() {
        return historyimagelink;
    }

    public void setHistoryimagelink(String historyimagelink) {
        this.historyimagelink = historyimagelink;
    }

    public String getHistorydescription() {
        return historydescription;
    }

    public void setHistorydescription(String historydescription) {
        this.historydescription = historydescription;
    }
}
