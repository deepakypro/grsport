package com.sport.grsport.Adapters;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sport.grsport.HelperClasses.BitmapTransform;
import com.sport.grsport.HelperClasses.CircleTransform;
import com.sport.grsport.HelperClasses.ImageLoader;
import com.sport.grsport.MainActivity;
import com.sport.grsport.Part3.DisplayShowGallery;
import com.sport.grsport.Part3.Home;
import com.sport.grsport.Part3.ShowGallery;
import com.sport.grsport.Part4.CreateUserProfile;
import com.sport.grsport.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import static com.sport.grsport.HelperClasses.Help.MAX_HEIGHT;
import static com.sport.grsport.HelperClasses.Help.MAX_WIDTH;


/**
 * Created by deepak on 13/02/17.
 */

public class LazyAdapter  extends BaseAdapter  {
   // private Context context;
    private List<ProfileRowItems> rowItems;
    int size;

    private Activity activity;

    public ImageLoader imageLoader;
    public   LazyAdapter(Activity context, List<ProfileRowItems> items) {
        this.activity = context;
        this.rowItems = items;

        imageLoader = new ImageLoader(context);
    }

    private class ViewHolder {
        //TextView name;
        ImageView imageView;
        RelativeLayout relativeLayout;
        CardView cardView;
        // ImageView image1;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        // ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_showimage, null);
            holder = new ViewHolder();
          //  holder.name = (TextView) convertView.findViewById(R.id.showimage_textview123);
            // holder.age = (TextView) convertView.findViewById(R.id.listview_team_member_age);
            holder.imageView = (ImageView) convertView.findViewById(R.id.showimage_imageview123);
            holder.cardView = (CardView) convertView.findViewById(R.id.showimage_cardview);
            holder.relativeLayout = (RelativeLayout) convertView.findViewById(R.id.showimage_download123);

            /// holder.distance = (TextView) convertView.findViewById(R.id.customlistworkoutdistance);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        //holder.date.setText(detail.getWorkoutdate());
        final ProfileRowItems detail = rowItems.get(position);
        // if(!date1.equals(detail.getWorkoutdate()))
        //{
        // detail = rowItems.get(position);
        //  date1="december2010";
        //date1=detail.getWorkoutdate();
        //countnumber=1;
        //holder.time.setText(detail.getWorkouttime());
        // holder.workoutname.setText(detail.getWorkouttime());
         size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
       // holder.name.setText(detail.getName());
        // holder.workoutnumber.setText(countnumber+"");
        //holder.workoutname.setText(detail.getWorkouttime());
        Picasso.with(activity)
                .load(detail.getImagelink())
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                .resize(size, size)
                .centerInside()
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        // Try again online if cache failed
                        Picasso.with(activity)
                                .load(detail.getImagelink())
                                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                                .resize(size, size)
                                .centerInside()
                                .into(holder.imageView);
                    }

                });
        Picasso.with(activity)
                .load(detail.getImagelink())
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))

                .resize(size, size)
                .centerInside()
                .into(holder.imageView);


        convertView.setTag(holder);
       // imageLoader.DisplayImage(detail.getImagelink(), holder.imageView);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*String filename = "bitmap.png";
               // BitmapDrawable drawable = (BitmapDrawable) holder.imageView.getDrawable();
                //Bitmap bitmap = drawable.getBitmap();

                holder.imageView.setDrawingCacheEnabled(true);
                holder.imageView.buildDrawingCache();
                Bitmap bitmap = Bitmap.createBitmap(holder.imageView.getDrawingCache());

                try {
                    //Write file

                    FileOutputStream stream = activity.openFileOutput(filename, Context.MODE_PRIVATE);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);

                    //Cleanup
                    stream.close();
                    bitmap.recycle();


                } catch (Exception e) {
                    e.printStackTrace();
                }

                if(Build.VERSION.SDK_INT>21)
                {

                    Bundle bundle = ActivityOptions.makeSceneTransitionAnimation(activity,
                            Pair.create(view,"selectedImage")
                    ).toBundle();

                    Intent intent = new Intent(activity,DisplayShowGallery.class);
                    intent.putExtra("bg",filename);
                    
                    //intent.putExtra("cover",DataModel.cover[i]);
                    //intent.putExtra("title",DataModel.movies[i]);
                    intent.putExtra("plot",detail.getName());
                    activity.startActivity(intent,bundle);
                }else {

                    Bundle bundle=new Bundle();
                    Intent intent = new Intent(activity,DisplayShowGallery.class);
                    intent.putExtra("bg",filename);
                    //intent.putExtra("cover",DataModel.cover[i]);
                    //intent.putExtra("title",DataModel.movies[i]);
                    intent.putExtra("plot",detail.getName());
                    activity.startActivity(intent,bundle);
                }


*/




// Loads given image






               final Dialog dialog=new Dialog(activity);
                dialog.setContentView(R.layout.dialog_showimage);
                dialog.setCancelable(true);
                dialog.show();
                // final ProgressBar progressBar=(ProgressBar) dialog.findViewById(R.id.dailog_progressBar);
                //progressBar.setVisibility(View.VISIBLE);


                ImageView imageView=(ImageView) dialog.findViewById(R.id.dialog_showimage);

                Picasso.with(activity)
                        .load(detail.getImagelink())
                        .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))
                        .skipMemoryCache()
                        .resize(size, size)
                        .centerInside()
                        .into(imageView);


            }
        });


        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadpdf(detail.getImagelink());
            }
        });


        return convertView;
    }


    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }



    public  void downloadpdf(String url){

        Random r = new Random();
        int i1 = r.nextInt(8000099 - 6503) + 65;
        Calendar c=Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String imagename = df.format(c.getTime())+i1;

        FirebaseStorage storage = FirebaseStorage.getInstance();
        //StorageReference  islandRef = mStorage.child("images/island.jpg");
        StorageReference httpsReference = storage.getReferenceFromUrl(url);

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, imagename+".jpg");

        try {
            // Very simple code to copy a picture from the application's
            // resource into the external file.  Note that this code does
            // no error checking, and assumes the picture is small (does not
            // try to copy it in chunks).  Note that if external storage is
            // not currently mounted this will silently fail.
            InputStream is = activity.getResources().openRawResource(+ R.drawable.a);
            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(activity,
                    new String[] { file.toString() }, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            Log.w("ExternalStorage", "Error writing " + file, e);
        }

        System.out.println("localFile=" + file.getAbsolutePath());
        httpsReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                Toast.makeText(activity,"Download Completed!!",Toast.LENGTH_SHORT).show();
                // Local temp file has been created
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }





}










