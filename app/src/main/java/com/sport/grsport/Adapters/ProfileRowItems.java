package com.sport.grsport.Adapters;

import com.sport.grsport.part1.userInformationClass;

/**
 * Created by deepak on 29/01/17.
 */

public class ProfileRowItems extends userInformationClass {
    String imagelink;

    public String getImagelink() {
        return imagelink;
    }

    public void setImagelink(String imagelink) {
        this.imagelink = imagelink;
    }

    @Override
    public String toString(){
        return getImagelink();
    }
}
