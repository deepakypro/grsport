package com.sport.grsport.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sport.grsport.HelperClasses.BitmapTransform;
import com.sport.grsport.HelperClasses.ImageLoader;
import com.sport.grsport.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.sport.grsport.HelperClasses.Help.MAX_HEIGHT;
import static com.sport.grsport.HelperClasses.Help.MAX_WIDTH;

/**
 * Created by deepak on 29/01/17.
 */

public class ProfileCustomAdapter extends RecyclerView.Adapter<ProfileCustomAdapter.MyViewHolder> {

    private List<ChildRowItems> horizontalList;

    int i=0;
    private Context context;
    public ImageLoader imageLoader;
    private OnItemClick mCallback;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name, age,count;
        ImageView imageView;
        RelativeLayout relativeLayout;


        public MyViewHolder(View convertView) {
            super(convertView);
            name = (TextView) convertView.findViewById(R.id.listview_team_member_name);
            count = (TextView) convertView.findViewById(R.id.listview_team_member_count);
            age = (TextView) convertView.findViewById(R.id.listview_team_member_age);
            imageView = (ImageView) convertView.findViewById(R.id.listview_team_member_profile);
            relativeLayout=(RelativeLayout) convertView.findViewById(R.id.listview_team_member_relative);

        }
    }


    public ProfileCustomAdapter(Context context ,List<ChildRowItems> horizontalList,OnItemClick listener) {
        this.context=context;
        this.mCallback = listener;
        this.horizontalList = horizontalList;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public ProfileCustomAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listview_team_members, parent, false);

        return new ProfileCustomAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProfileCustomAdapter.MyViewHolder holder, final int position) {
        final ChildRowItems detail = horizontalList.get(position);

        i++;
       // imageLoader.DisplayImage(detail.getImagelink(), holder.imageView);

        holder.count.setText(i+"");

        int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

        Picasso.with(context)
                .load(detail.getImagelink())
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))

                .resize(size, size)
                .centerInside()
                .into(holder.imageView);
        holder.name.setText(detail.getName());
        holder.age.setText(detail.getAge());
      //  Picasso.with(context).load(detail.getImagelink()).transform(new CircleTransform()).into(holder.imageView);
        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String ItemName = detail.getChilduser()+"$$$$$$$"+detail.getImagelink();

                increment(ItemName);

            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }

    public interface OnItemClick {
        void onClick (String value);
    }

    public void increment(String string){

        mCallback.onClick(string);
    }


}















        /*extends BaseAdapter {
    private Context context;
    private List<ChildRowItems> rowItems;


  public   ProfileCustomAdapter(Context context, List<ChildRowItems> items) {
        this.context = context;
        this.rowItems = items;
    }

    private class ViewHolder {
        TextView name, age;
        ImageView imageView;

        // ImageView image1;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        // ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_listview_team_members, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.listview_team_member_name);
            holder.age = (TextView) convertView.findViewById(R.id.listview_team_member_age);
            holder.imageView = (ImageView) convertView.findViewById(R.id.listview_team_member_profile);

            /// holder.distance = (TextView) convertView.findViewById(R.id.customlistworkoutdistance);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        //holder.date.setText(detail.getWorkoutdate());
        ChildRowItems detail = rowItems.get(position);
        // if(!date1.equals(detail.getWorkoutdate()))
        //{
        // detail = rowItems.get(position);
        //  date1="december2010";
        //date1=detail.getWorkoutdate();
        //countnumber=1;
        //holder.time.setText(detail.getWorkouttime());
        // holder.workoutname.setText(detail.getWorkouttime());

        holder.name.setText(detail.getName());
        // holder.workoutnumber.setText(countnumber+"");
        //holder.workoutname.setText(detail.getWorkouttime());

        convertView.setTag(holder);
        //}else {
        // detail = rowItems.get(position);
        // holder.time.setText(detail.getWorkouttime());
        holder.age.setText(detail.getAge());
        Picasso.with(context).load(detail.getImagelink()).transform(new CircleTransform()).into(holder.imageView);
        /*Picasso.with(context).load(detail.getImagelink()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                RoundedBitmapDrawable drawable = Help.createRoundedBitmapDrawableWithBorder(bitmap);


                holder.imageView.setImageDrawable(drawable);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });*/


        //countnumber+=1;
        //holder.workoutnumber.setText(countnumber+"");
        //convertView.setTag(holder);

        //  }



        // holder.duration.setText(function.getDuration(detail.getDuration()));
        // Bitmap albumArt = function.getAlbumart(context, detail.getAlbumId());
        // holder.image1.setBackgroundDrawable(new BitmapDrawable(albumArt));
        // holder.image1.setImageResource(new BitmapDrawable(albumArt));
      /*  return convertView;
    }


    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}*/


