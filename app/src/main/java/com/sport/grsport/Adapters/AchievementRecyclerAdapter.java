package com.sport.grsport.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.sport.grsport.HelperClasses.BitmapTransform;
import com.sport.grsport.HelperClasses.ImageLoader;
import com.sport.grsport.Part3.ShowGallery;
import com.sport.grsport.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.sport.grsport.HelperClasses.Help.MAX_HEIGHT;
import static com.sport.grsport.HelperClasses.Help.MAX_WIDTH;
import static com.sport.grsport.HelperClasses.Help.isNetworkStatusAvialable;

/**
 * Created by deepak on 03/06/17.
 */

public class AchievementRecyclerAdapter extends RecyclerView.Adapter<AchievementRecyclerAdapter.MyViewHolder> {

    private List<ProfileRowItems> horizontalList;

    private Context context;
    public ImageLoader imageLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;

        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.threeimage_imageview123);

        }
    }


    public AchievementRecyclerAdapter(Context context ,List<ProfileRowItems> horizontalList) {
        this.context=context;
        this.horizontalList = horizontalList;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public AchievementRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listview_threeimage, parent, false);

        return new AchievementRecyclerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AchievementRecyclerAdapter.MyViewHolder holder, final int position) {
        final ProfileRowItems detail = horizontalList.get(position);

        int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

        Picasso.with(context)
                .load(detail.getImagelink())
                .transform(new BitmapTransform(MAX_WIDTH, MAX_HEIGHT))

                .resize(size, size)
                .centerInside()
                .into(holder.imageView);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isNetworkStatusAvialable(context)) {
                    Intent intent = new Intent(context, ShowGallery.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("gallery", "Achievement");
                    context.startActivity(intent);

                }else {
                    Toast.makeText(context,"No Internet Connection !!",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}




