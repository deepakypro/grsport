package com.sport.grsport.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sport.grsport.HelperClasses.CircleTransform;
import com.sport.grsport.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by deepak on 01/02/17.
 */

public class ShowImageCustomAdapter  extends BaseAdapter {
    private Context context;
    private List<ProfileRowItems> rowItems;


    public   ShowImageCustomAdapter(Context context, List<ProfileRowItems> items) {
        this.context = context;
        this.rowItems = items;
    }

    private class ViewHolder {
       //TextView name;
        ImageView imageView;

        // ImageView image1;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        // ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_showimage, null);
            holder = new ViewHolder();
            //holder.name = (TextView) convertView.findViewById(R.id.showimage_textview123);
            // holder.age = (TextView) convertView.findViewById(R.id.listview_team_member_age);
            holder.imageView = (ImageView) convertView.findViewById(R.id.showimage_imageview123);

            /// holder.distance = (TextView) convertView.findViewById(R.id.customlistworkoutdistance);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        //holder.date.setText(detail.getWorkoutdate());
       final ProfileRowItems detail = rowItems.get(position);
        // if(!date1.equals(detail.getWorkoutdate()))
        //{
        // detail = rowItems.get(position);
        //  date1="december2010";
        //date1=detail.getWorkoutdate();
        //countnumber=1;
        //holder.time.setText(detail.getWorkouttime());
        // holder.workoutname.setText(detail.getWorkouttime());

      //  holder.name.setText(detail.getName());
        // holder.workoutnumber.setText(countnumber+"");
        //holder.workoutname.setText(detail.getWorkouttime());

        convertView.setTag(holder);
        //}else {
        // detail = rowItems.get(position);
        // holder.time.setText(detail.getWorkouttime());
        // holder.age.setText(detail.getAge());
        //Picasso.with(context).load(detail.getImagelink()).fit().into(holder.imageView);
        Picasso.with(context)
                .load(detail.getImagelink())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .transform(new CircleTransform())
                .into(holder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        // Try again online if cache failed
                        Picasso.with(context)
                                .load(detail.getImagelink())
                                .transform(new CircleTransform())
                                .into(holder.imageView);
                    }

                });
        /*Picasso.with(context).load(detail.getImagelink()).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                RoundedBitmapDrawable drawable = Help.createRoundedBitmapDrawableWithBorder(bitmap);


                holder.imageView.setImageDrawable(drawable);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });*/


        //countnumber+=1;
        //holder.workoutnumber.setText(countnumber+"");
        //convertView.setTag(holder);

        //  }



        // holder.duration.setText(function.getDuration(detail.getDuration()));
        // Bitmap albumArt = function.getAlbumart(context, detail.getAlbumId());
        // holder.image1.setBackgroundDrawable(new BitmapDrawable(albumArt));
        // holder.image1.setImageResource(new BitmapDrawable(albumArt));
        return convertView;
    }


    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}


