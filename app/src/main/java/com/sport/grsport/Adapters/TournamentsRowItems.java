package com.sport.grsport.Adapters;

import com.sport.grsport.HelperClasses.TournamentDetailsClass;

/**
 * Created by deepak on 13/03/17.
 */

public class TournamentsRowItems  extends TournamentDetailsClass{

    public String TournamentStatus;

    public String getIsTournamentRegistrationClosed() {
        return isTournamentRegistrationClosed;
    }

    public void setIsTournamentRegistrationClosed(String isTournamentRegistrationClosed) {
        this.isTournamentRegistrationClosed = isTournamentRegistrationClosed;
    }

    public String isTournamentRegistrationClosed;

    public String getTournamentStatus() {
        return TournamentStatus;
    }

    public void setTournamentStatus(String tournamentStatus) {
        TournamentStatus = tournamentStatus;
    }

    @Override
    public String toString(){
        return getTournamentUniqueCode();
    }

}
