package com.sport.grsport.Part3;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sport.grsport.Adapters.HistoryCustomAdapter;
import com.sport.grsport.Adapters.HistoryRowItems;
import com.sport.grsport.Adapters.LazyAdapter;
import com.sport.grsport.Adapters.ProfileRowItems;
import com.sport.grsport.R;

import java.util.ArrayList;
import java.util.List;

import static com.sport.grsport.HelperClasses.Help.firebase;

public class History extends AppCompatActivity {




    List<HistoryRowItems> rowItems;
    FirebaseStorage storage;
    ListView listView;
    private StorageReference storageReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        rowItems=new ArrayList<>();
        Firebase.setAndroidContext(this);
        storage = FirebaseStorage.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        FetchImage();

        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }

    }

    public void onclickhistorypreviousactivity(View view){
        startActivity(new Intent(getApplicationContext(),Home.class));
       finish();
    }

    private void FetchImage() {


        final ProgressDialog progressDialog=new ProgressDialog(History.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        firebase.child("History").child("clubhistory").child("Starting").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String value = (String) dataSnapshot.child("historyeventname").getValue();
                String value1 = (String) dataSnapshot.child("historyimagelink").getValue();
                String value2 = (String) dataSnapshot.child("historydate").getValue();
                String value3 = (String) dataSnapshot.child("historydescription").getValue();

                HistoryRowItems item = new HistoryRowItems();

                item.setHistoryeventname(value);
                item.setHistoryimagelink(value1);

                item.setHistorydate(value2);
                item.setHistorydescription(value3);

                progressDialog.dismiss();
                rowItems.add(item);

                if(rowItems.size()>0){
                    listView=(ListView)findViewById(R.id.historylistview);
                    HistoryCustomAdapter adapter = new HistoryCustomAdapter(getApplicationContext(), rowItems);
                    listView.setAdapter(adapter);
                    //listView.setOnItemClickListener(History.this);

                    // Log.d("hello",value+"hello"+item.getImagelink());

                }else {

                    progressDialog.dismiss();
                    // Log.d("hello","hello");
                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }




    @Override
    public void onBackPressed () {
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }

    public void onclickshowgalleryprevious(View view){
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }
}
