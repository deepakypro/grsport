package com.sport.grsport.Part3;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import com.sport.grsport.Adapters.LazyAdapter;
import com.sport.grsport.Adapters.ProfileCustomAdapter;
import com.sport.grsport.Adapters.ProfileRowItems;
import com.sport.grsport.Adapters.ShowImageCustomAdapter;
import com.sport.grsport.Adapters.ThreeImageCustomAdapter;
import com.sport.grsport.HelperClasses.CircleTransform;
import com.sport.grsport.HelperClasses.ImageUploadClass;
import com.sport.grsport.Part4.Profile;
import com.sport.grsport.R;
import com.sport.grsport.part1.login;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.sport.grsport.HelperClasses.Help.firebase;

public class ShowGallery extends AppCompatActivity  {

    List<ProfileRowItems> rowItems;
     FirebaseStorage storage;
    GridView gridView;
    String gallery;
    LazyAdapter adapter;
    private StorageReference storageReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_gallery);
        rowItems=new ArrayList<>();
        Firebase.setAndroidContext(this);
        storage = FirebaseStorage.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
                        startActivity(new Intent(getApplicationContext(),login.class));
            finish();
        }

        Bundle bundle=getIntent().getExtras();
        if(bundle==null)
            return;
        gallery=bundle.getString("gallery");

        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }
        FetchImage();
    }

    private void FetchImage() {
        final ProgressDialog progressDialog=new ProgressDialog(ShowGallery.this);
        progressDialog.setCancelable(false);
        progressDialog.show();
        firebase.child("Images").child("Gallery").child(gallery).child("TournamentGallery").orderByChild("show").equalTo("yes").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String value = (String) dataSnapshot.child("image").getValue();
                String value1 = (String) dataSnapshot.child("description").getValue();

                ProfileRowItems item = new ProfileRowItems();

                item.setImagelink(value);
                item.setName(value1);

                progressDialog.dismiss();
                rowItems.add(item);

                if(rowItems.size()>0){
                    gridView = (GridView) findViewById(R.id.gridview_showGallery);
                     adapter = new LazyAdapter(ShowGallery.this, rowItems);
                    gridView.setAdapter(adapter);



                   // Log.d("hello",value+"hello"+item.getImagelink());

                }else {

                    progressDialog.dismiss();
                   // Log.d("hello","hello");
                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }



   /* @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Object obj = gridView.getAdapter().getItem(position);
        String link=obj.toString();
        Toast.makeText(getApplicationContext(), "Item " +link, Toast.LENGTH_SHORT).show();
        showImage(link);


    }*/




    public  void downloadpdf(String url){

        //StorageReference  islandRef = mStorage.child("images/island.jpg");
        StorageReference httpsReference = storage.getReferenceFromUrl(url);

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(path, "DemoPicture.jpg");

        try {
            // Very simple code to copy a picture from the application's
            // resource into the external file.  Note that this code does
            // no error checking, and assumes the picture is small (does not
            // try to copy it in chunks).  Note that if external storage is
            // not currently mounted this will silently fail.
            InputStream is = getResources().openRawResource(+ R.drawable.a);
            OutputStream os = new FileOutputStream(file);
            byte[] data = new byte[is.available()];
            is.read(data);
            os.write(data);
            is.close();
            os.close();

            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(this,
                    new String[] { file.toString() }, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            Log.w("ExternalStorage", "Error writing " + file, e);
        }

        System.out.println("localFile=" + file.getAbsolutePath());
        httpsReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                System.out.println("kudos");
                // Local temp file has been created
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
       // adapter.imageLoader.clearCache();
        //adapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed () {
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }

    public void onclickshowgalleryprevious(View view){
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }

    private void showImage(String link){

        final Dialog dialog=new Dialog(ShowGallery.this);
        dialog.setContentView(R.layout.dialog_showimage);
        dialog.setCancelable(true);
        dialog.show();
       // final ProgressBar progressBar=(ProgressBar) dialog.findViewById(R.id.dailog_progressBar);
        //progressBar.setVisibility(View.VISIBLE);


        ImageView imageView=(ImageView) dialog.findViewById(R.id.dialog_showimage);
        Picasso.with(ShowGallery.this)
                .load(link)

                .into(imageView);

       // Toast.makeText(getApplicationContext(),link+" ",Toast.LENGTH_SHORT).show();
       // progressBar.setVisibility(View.GONE);
        /*WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);*/


       /* ImageView closeimageView=(ImageView) dialog.findViewById(R.id.dialog_closewindow);
        closeimageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
*/



    }


}
