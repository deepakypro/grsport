package com.sport.grsport.Part3;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sport.grsport.Adapters.AchievementRecyclerAdapter;
import com.sport.grsport.Adapters.ActiveCustomAdapter;
import com.sport.grsport.Adapters.HorizontalCustomAdapter;
import com.sport.grsport.Adapters.ProfileRowItems;
import com.sport.grsport.Adapters.RecyclerAdapter;
import com.sport.grsport.Adapters.RegisteredTournament;
import com.sport.grsport.Adapters.ThreeImageCustomAdapter;
import com.sport.grsport.Adapters.TournamentsRowItems;
import com.sport.grsport.HelperClasses.CircleTransform;
import com.sport.grsport.HelperClasses.SharedPreference;
import com.sport.grsport.HelperClasses.SlidingImage_Adapter;
import com.sport.grsport.MainActivity;
import com.sport.grsport.Part2.ContactUs;
import com.sport.grsport.Part2.EnterInformation;
import com.sport.grsport.Part4.MyCreateTeam;
import com.sport.grsport.Part4.Profile;
import com.sport.grsport.Part5.ActiveTournament;
import com.sport.grsport.R;
import com.sport.grsport.SharedPreferenced.usersession;
import com.sport.grsport.part1.login;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static com.sport.grsport.HelperClasses.Help.firebase;
import static com.sport.grsport.HelperClasses.Help.isNetworkStatusAvialable;

public class Home extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final int PERMISSION_REQUEST_CODE = 200;
    private GridLayoutManager lLayout;;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    usersession Usersession;
    TextView textView_profilename;
    ImageView imageView_profileimage;
     Handler handler;
    int currImage=0;
     Runnable r;
    ListView  Activetournamentlistview;
    FirebaseStorage storage;
    SharedPreference sharedPreference;
    String username,imagelink,checkusername;
    private FirebaseAuth auth;
    ListView listView;
    GridView gridView,gridView1;
    private StorageReference storageReference;
    List<ProfileRowItems> rowItems_GUESTGALLERY,ROWITEMS_TOURNAMENTGALLERY,rowitems_sponsor,rowItems_ACHIEVEMENT;
    List<RegisteredTournament> ListView_drawer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        Firebase.setAndroidContext(this);
        auth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        sharedPreference=new SharedPreference(getApplicationContext());
        storageReference = FirebaseStorage.getInstance().getReference();
        rowItems_GUESTGALLERY=new ArrayList<>();
        rowItems_ACHIEVEMENT=new ArrayList<>();
        rowitems_sponsor=new ArrayList<>();
        ROWITEMS_TOURNAMENTGALLERY=new ArrayList<>();
        ListView_drawer=new ArrayList<>();

        Usersession=new usersession(getApplicationContext());

        if(checkPermission()){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {


            startActivity(new Intent(getApplicationContext(),login.class));
            finish();
        }

        if(!sharedPreference.checkLogin())
        {

            LinearLayout linearLayout=(LinearLayout) findViewById(R.id.homehome);
            linearLayout.setVisibility(View.GONE);

            HashMap<String,String> user1=Usersession.getUserDetails();
            String  usern=user1.get(Usersession.KEY_USERNAME);

            firebase.child("userdetails").child(usern).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String  imageurl=null,age=null,city=null;
                      imageurl = (String) dataSnapshot.child("image").getValue();
                     age=(String) dataSnapshot.child("age").getValue();
                     city=(String) dataSnapshot.child("city").getValue();
                    String contactnumber=(String) dataSnapshot.child("contactnumber").getValue();



                    if(imageurl!=null && age!=null && city!=null)
                    {
                        startActivity(new Intent(getApplicationContext(),login.class));
                        finish();
                    }else {
                        startActivity(new Intent(getApplicationContext(),EnterInformation.class));
                        finish();
                    }



                }
                @Override public void onCancelled(FirebaseError error) { }
            });




        }else {
            setText();


            setprofile();
            getimageRotateImagelink();

            getDATA();
            SETGUESTGALLERYIMAGE();

          displayads();

           AsyncTaskRunner runner = new AsyncTaskRunner();

            runner.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        }


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),History.class));
                finish();
               // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();




        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }
        }else {
            requestPermission();
        }

    }

    private void getimageRotateImagelink(){
        final ProgressBar progressBar=(ProgressBar) findViewById(R.id.progressBar2);
        progressBar.setVisibility(View.VISIBLE);
        firebase.child("HomeImage").child("Rotate").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               // final String value = (String) dataSnapshot.child("image").getValue();
                //final String value1 = (String) dataSnapshot.child("image1").getValue();
                //final String value2 = (String) dataSnapshot.child("image2").getValue();
                // Toast.makeText(Home.this,value,Toast.LENGTH_SHORT).show();

               ArrayList<String> arrayList=(ArrayList<String>) dataSnapshot.child("registerArrayList").getValue();

                //Toast.makeText(getApplicationContext(),arrayList+"fsd",Toast.LENGTH_SHORT).show();

                progressBar.setVisibility(View.GONE);
                init(arrayList);
            }


            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private void init(ArrayList<String> IMAGES) {


        mPager = (ViewPager) findViewById(R.id.pager);


        mPager.setAdapter(new SlidingImage_Adapter(Home.this,IMAGES));


        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES =IMAGES.size();

        // Auto start of viewpager
       /* final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);*/

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }




    private void getDATA(){
        final ProgressBar progressBar=(ProgressBar)findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.VISIBLE);

        final TextView textView=(TextView) findViewById(R.id.hometextactivetournament);
        final CardView cardView=(CardView)findViewById(R.id.homecardviewactivetournament);
        final List<TournamentsRowItems> ActivetournamentsRowItemses=new ArrayList<>();
        firebase.child("tournaments").orderByChild("tournamentISOVER").equalTo("No").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String name = (String) dataSnapshot.child("tournamentName").getValue();
                String date = (String) dataSnapshot.child("tournamentFrom").getValue();
                String status = (String) dataSnapshot.child("TournamentStatus").getValue();
                String id = (String) dataSnapshot.child("tournamentUniqueCode").getValue();


                TournamentsRowItems item = new TournamentsRowItems();
                item.setTournamentName(name);
                item.setTournamentFrom(date);
                item.setTournamentUniqueCode(id);
                item.setTournamentStatus(status);


                Log.d("home",ActivetournamentsRowItemses.size()+""+date);
                ActivetournamentsRowItemses.add(item);

                if(ActivetournamentsRowItemses.size()>0){

                    cardView.setVisibility(View.VISIBLE);
                    textView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    Activetournamentlistview = (ListView) findViewById(R.id.listviewactiveTournament);
                    ActiveCustomAdapter adapter = new ActiveCustomAdapter(getApplicationContext(), ActivetournamentsRowItemses);
                    Activetournamentlistview.setAdapter(adapter);
                    Activetournamentlistview.setOnItemClickListener(Home.this);
                    ContactUs.ListUtils.setDynamicHeight(Activetournamentlistview);



                }else {
                    cardView.setVisibility(View.GONE);
                    textView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    private void setprofile(){
        // Firebase ref = new Firebase("https://showursport-b6e37.firebaseio.com/userdetails/");
        firebase.child("userdetails").child(username).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String value = (String) dataSnapshot.child("image").getValue();
                // Toast.makeText(Home.this,value,Toast.LENGTH_SHORT).show();


               // int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

                Picasso.with(Home.this)
                        .load(value)
                        .error(R.drawable.noprofilepicture)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .transform(new CircleTransform())
                        .into(imageView_profileimage, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                // Try again online if cache failed
                                Picasso.with(Home.this)
                                        .load(value)
                                        .transform(new CircleTransform())
                                        .into(imageView_profileimage);
                            }

                        });
            }


            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }


    /////
    /////










    private void SETGUESTGALLERYIMAGE(){

        final ProgressBar progressBar=(ProgressBar)findViewById(R.id.progressBar1);
        progressBar.setVisibility(View.VISIBLE);

        firebase.child("Images/Gallery/GuestGallery/TournamentGallery").orderByChild("showonhome").equalTo("yes").limitToFirst(3).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String value = (String) dataSnapshot.child("image").getValue();


                ProfileRowItems item = new ProfileRowItems();

                item.setImagelink(value);


                rowItems_GUESTGALLERY.add(item);






                if(rowItems_GUESTGALLERY.size()>0){
                    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(Home.this, LinearLayoutManager.HORIZONTAL, false);

                    RecyclerView   horizontal_recycler_view= (RecyclerView) findViewById(R.id.horizontal_recycler_view);
                    HorizontalCustomAdapter      horizontalAdapter=new HorizontalCustomAdapter(getApplicationContext(),rowItems_GUESTGALLERY);
                     horizontal_recycler_view.setHasFixedSize(true);

                    horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);

                    horizontal_recycler_view.setAdapter(horizontalAdapter);


                   // gridView = (GridView) findViewById(R.id.home_listview_tournamentgallery);
                     //ThreeImageCustomAdapter adapter = new ThreeImageCustomAdapter(getApplicationContext(), rowItems_GUESTGALLERY);
                    //gridView.setAdapter(adapter);
                    //gridView.setOnItemClickListener(Home.this);


                    progressBar.setVisibility(View.GONE);
                }else {
                    progressBar.setVisibility(View.GONE);

                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }


    private void SETACHIEVEMENTGALLERYIMAGE(){

        final ProgressBar progressBar=(ProgressBar)findViewById(R.id.progressBar5);
        progressBar.setVisibility(View.VISIBLE);

        firebase.child("Images/Gallery/Achievement/TournamentGallery").orderByChild("showonhome").equalTo("yes").limitToFirst(3).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String value = (String) dataSnapshot.child("image").getValue();


                ProfileRowItems item = new ProfileRowItems();

                item.setImagelink(value);


                rowItems_ACHIEVEMENT.add(item);






                if(rowItems_ACHIEVEMENT.size()>0){
                    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(Home.this, LinearLayoutManager.HORIZONTAL, false);

                    RecyclerView   horizontal_recycler_view= (RecyclerView) findViewById(R.id.home_listview_achievement);
                    AchievementRecyclerAdapter horizontalAdapter=new AchievementRecyclerAdapter(getApplicationContext(),rowItems_ACHIEVEMENT);
                    horizontal_recycler_view.setHasFixedSize(true);

                    horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);

                    horizontal_recycler_view.setAdapter(horizontalAdapter);


                    // gridView = (GridView) findViewById(R.id.home_listview_tournamentgallery);
                    //ThreeImageCustomAdapter adapter = new ThreeImageCustomAdapter(getApplicationContext(), rowItems_GUESTGALLERY);
                    //gridView.setAdapter(adapter);
                    //gridView.setOnItemClickListener(Home.this);


                    progressBar.setVisibility(View.GONE);
                }else {
                    progressBar.setVisibility(View.GONE);

                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        Sponsorgridview();
    }



    private void GALLERYIMAGE(){
       final ProgressBar progressBar=(ProgressBar)findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        firebase.child("Images/Gallery/Tournament/TournamentGallery").orderByChild("showonhome").equalTo("yes").limitToFirst(3).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String value = (String) dataSnapshot.child("image").getValue();
                String value1 = (String) dataSnapshot.child("date").getValue();

                ProfileRowItems item = new ProfileRowItems();

                item.setImagelink(value);
                item.setName(value1);

                ROWITEMS_TOURNAMENTGALLERY.add(item);

                if(ROWITEMS_TOURNAMENTGALLERY.size()>0){

                    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(Home.this, LinearLayoutManager.HORIZONTAL, false);

                    RecyclerView   horizontal_recycler_view= (RecyclerView) findViewById(R.id.home_listview_tournament);
                    ThreeImageCustomAdapter      horizontalAdapter=new ThreeImageCustomAdapter(getApplicationContext(),ROWITEMS_TOURNAMENTGALLERY);
                        horizontal_recycler_view.setHasFixedSize(true);
                    horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);

                    horizontal_recycler_view.setAdapter(horizontalAdapter);



                    progressBar.setVisibility(View.GONE);
                }else {
                    progressBar.setVisibility(View.GONE);
                    Log.d("hello","hello");
                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


}



    private void LiveTournament(){

        final TextView textView=(TextView) findViewById(R.id.home_live_textview);
        final ImageView imageView=(ImageView) findViewById(R.id.home_live_tournament);

        firebase.child("Images/Gallery/LiveTournament/TournamentGallery").orderByChild("showonhome").equalTo("yes").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                // String value = (String) dataSnapshot.child("image").getValue();
                String value1 = (String) dataSnapshot.child("image").getValue();


               if(value1!=null)
               {
                   imageView.setVisibility(View.VISIBLE);
                   textView.setVisibility(View.VISIBLE);
                   Picasso.with(Home.this)
                           .load(value1)
                           .into(imageView);

                   imageView.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {

                           if (isNetworkStatusAvialable(getApplicationContext())) {
                               Intent intent = new Intent(getApplicationContext(), ShowGallery.class);
                               intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                               intent.putExtra("gallery", "LiveTournament");
                               startActivity(intent);
                           }else {
                               Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
                           }
                       }
                   });
               }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }


    private void Sponsorgridview(){
        final ProgressBar progressBar=(ProgressBar)findViewById(R.id.progressBar4);
        progressBar.setVisibility(View.VISIBLE);

        firebase.child("Sponsor").child("SponsorGallery").orderByChild("valid").equalTo("yes").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
               // String value = (String) dataSnapshot.child("image").getValue();
                String value1 = (String) dataSnapshot.child("name").getValue();

                ProfileRowItems item = new ProfileRowItems();

                //item.setImagelink(value);
                item.setName(value1);

                rowitems_sponsor.add(item);

                if(rowitems_sponsor.size()>0){

                    TextView textView=(TextView) findViewById(R.id.hidetextviewsponsor);
                    textView.setVisibility(View.VISIBLE);
                    CardView cardView=(CardView) findViewById(R.id.hidecardviewsponsor);
                    cardView.setVisibility(View.VISIBLE);


                    GridView   horizontal_recycler_view= (GridView) findViewById(R.id.home_listview_sponsor);
                    RecyclerAdapter horizontalAdapter=new RecyclerAdapter(getApplicationContext(),rowitems_sponsor);

                    horizontal_recycler_view.setAdapter(horizontalAdapter);

                    ListUtils.setDynamicHeight(horizontal_recycler_view);


                    progressBar.setVisibility(View.GONE);
                }else {
                    progressBar.setVisibility(View.GONE);
                    Log.d("hello","hello");
                }



            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        LiveTournament();
    }



    private void setDynamicHeight(GridView gridView) {
        ListAdapter gridViewAdapter = gridView.getAdapter();
        if (gridViewAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = gridViewAdapter.getCount();
        int rows = 0;

        View listItem = gridViewAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > 5 ){
            x = items/5;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);
    }

    public static class ListUtils {
        public static void setDynamicHeight(GridView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getColumnWidth() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        if(parent.getId()==R.id.listviewactiveTournament){
            Object obj = Activetournamentlistview.getAdapter().getItem(position);
            String code=obj.toString();
            Intent intent=new Intent(getApplicationContext(),ActiveTournament.class);
            intent.putExtra("uniqueid",code);
            startActivity(intent);

        }



    }




    public void setText(){

        View view=findViewById(R.id.home_layout);
        imageView_profileimage=(ImageView)view.findViewById(R.id.home_navigation_profile_picture);
        textView_profilename=(TextView) view.findViewById(R.id.home_navigation_profile_name);
        TextView profile_menu=(TextView) view.findViewById(R.id.home_navigation_profile);
        TextView tournament_menu=(TextView) view.findViewById(R.id.home_navigation_tournaments);
        TextView guest_menu=(TextView) view.findViewById(R.id.home_navigation_guest_gallery);
        TextView about_menu=(TextView) view.findViewById(R.id.home_navigation_about_us);
        TextView logout_menu=(TextView) view.findViewById(R.id.home_navigation_logout);
        TextView showmyteam=(TextView) view.findViewById(R.id.home_navigation_my_team);
        TextView contactus=(TextView) view.findViewById(R.id.home_navigation_contact_us);
        TextView privacy=(TextView) view.findViewById(R.id.home_navigation_privacy);
        profile_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkStatusAvialable(getApplicationContext())) {
                    startActivity(new Intent(getApplicationContext(), Profile.class));
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        tournament_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             Intent intent=new Intent(getApplicationContext(),ShowGallery.class);
                if (isNetworkStatusAvialable(getApplicationContext())) {
                    intent.putExtra("gallery", "Tournament");
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        guest_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkStatusAvialable(getApplicationContext())) {
                    Intent intent = new Intent(getApplicationContext(), ShowGallery.class);
                    intent.putExtra("gallery", "GuestGallery");
                    startActivity(intent);
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        about_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        logout_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkStatusAvialable(getApplicationContext())) {

                    FirebaseAuth.getInstance().signOut();
                    sharedPreference.logoutUser();
                    startActivity(new Intent(Home.this, login.class));
                    finish();


                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
                }

            }
        });

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://sites.google.com/view/grsport/home"));
                startActivity(browserIntent);
            }
        });

        contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkStatusAvialable(getApplicationContext())) {



                    startActivity(new Intent(Home.this, ContactUs.class));
                    finish();


                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
                }

            }
        });

        showmyteam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isNetworkStatusAvialable(getApplicationContext())) {
                    startActivity(new Intent(Home.this, MyCreateTeam.class));
                    finish();
                }else {
                    Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
                }
            }
        });
        HashMap<String,String> user=sharedPreference.getUserDetails();
        username=user.get(sharedPreference.KEY_USERNAME);
        String name=user.get(sharedPreference.KEY_NAME);
        textView_profilename.setText(name);

        String singleorTeam=user.get(sharedPreference.KEY_TYPE);
        if(singleorTeam.equals("Single Registration")){
            showmyteam.setVisibility(View.GONE);
        }else if(singleorTeam.equals("Team Registration")){
            showmyteam.setVisibility(View.VISIBLE);
        }

    }






    public void onclickactivitypartner(View view){
        Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=activity.com.myactivity2");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


 public void onclick_menu_show(View view){
     DrawerLayout  drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
     drawer.openDrawer(GravityCompat.START);
 }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        } else {
            super.onBackPressed();
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }


    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{CAMERA, READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean readexternalstorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted && readexternalstorage){
                        startActivity(new Intent(this, MainActivity.class));
                        //finish();
                    }
                    // Snackbar.make(view, "Permission Granted, Now you can access location data and camera.", Snackbar.LENGTH_LONG).show();
                    else {

                        //Snackbar.make(view, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA, READ_EXTERNAL_STORAGE},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(Home.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        //handler.removeCallbacks(r);

    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {


        String count;

        @Override
        protected String doInBackground(String... params) {

            GALLERYIMAGE();
            SETACHIEVEMENTGALLERYIMAGE();

            return count;
        }


        @Override
        protected void onPostExecute(String result) {

        }


        @Override
        protected void onPreExecute() {

        }


        @Override
        protected void onProgressUpdate(String... text) {


        }
    }

    // TEST ID
    private static final String ADMOB_AD_UNIT_ID = "ca-app-pub-3154335203110011/7883336483";
    private static final String ADMOB_APP_ID = "ca-app-pub-3154335203110011~1197674481";


    ////ads
    private void displayads()
    {

        NativeExpressAdView adView = (NativeExpressAdView) findViewById(R.id.adView);
        adView.loadAd(new AdRequest.Builder().build());

       /* MobileAds.initialize(this, ADMOB_APP_ID);

        AdLoader.Builder builder = new AdLoader.Builder(this, ADMOB_AD_UNIT_ID);

        // Add App Install Advertise
        builder.forAppInstallAd(new NativeAppInstallAd.OnAppInstallAdLoadedListener() {
            @Override
            public void onAppInstallAdLoaded(NativeAppInstallAd ad) {
                FrameLayout frameLayout =
                        (FrameLayout) findViewById(R.id.fl_adplaceholder);
                NativeAppInstallAdView adView = (NativeAppInstallAdView) getLayoutInflater()
                        .inflate(R.layout.ad_app_install, null);
                populateAppInstallAdView(ad, adView);
                frameLayout.removeAllViews();
                frameLayout.addView(adView);
            }
        });


        AdLoader adLoader = builder.withAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                Toast.makeText(Home.this, "Failed to load native ad: "
                        + errorCode, Toast.LENGTH_SHORT).show();
            }
        }).build();


        adLoader.loadAd(new AdRequest.Builder().build());*/
    }

    private void populateAppInstallAdView(NativeAppInstallAd nativeAppInstallAd,
                                          NativeAppInstallAdView adView) {


        adView.setHeadlineView(adView.findViewById(R.id.appinstall_headline));
        adView.setImageView(adView.findViewById(R.id.appinstall_image));
        adView.setBodyView(adView.findViewById(R.id.appinstall_body));
        adView.setCallToActionView(adView.findViewById(R.id.appinstall_call_to_action));
        adView.setIconView(adView.findViewById(R.id.appinstall_app_icon));
        adView.setPriceView(adView.findViewById(R.id.appinstall_price));
        adView.setStarRatingView(adView.findViewById(R.id.appinstall_stars));
        adView.setStoreView(adView.findViewById(R.id.appinstall_store));

        // Some assets are guaranteed to be in every NativeAppInstallAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAppInstallAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAppInstallAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAppInstallAd.getCallToAction());
        ((ImageView) adView.getIconView()).setImageDrawable(nativeAppInstallAd.getIcon()
                .getDrawable());

        List<NativeAd.Image> images = nativeAppInstallAd.getImages();

        if (images.size() > 0) {
            ((ImageView) adView.getImageView()).setImageDrawable(images.get(0).getDrawable());
        }

        // Some aren't guaranteed, however, and should be checked.
        if (nativeAppInstallAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAppInstallAd.getPrice());
        }

        if (nativeAppInstallAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAppInstallAd.getStore());
        }

        if (nativeAppInstallAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAppInstallAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAppInstallAd);
    }


}
