package com.sport.grsport.Part3;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.transition.Transition;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.sport.grsport.R;
import com.squareup.picasso.Picasso;

import java.io.FileInputStream;

public class DisplayShowGallery extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_show_gallery);

        Firebase.setAndroidContext(this);
        Intent intent = getIntent();
        CardView cardView = (CardView) findViewById(R.id.card_view);
        ImageView image = (ImageView) findViewById(R.id.displayshowimage);
        TextView details = (TextView) findViewById(R.id.displayshowdetails);
        //ImageView movie_bg = (ImageView) findViewById(R.id.cover_bg_details);
        //TextView plot = (TextView) findViewById(R.id.txt_plot_details);

        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }
        if(Build.VERSION.SDK_INT>21) {

            //These are lines helping Details_Card To Animate
            //===============================================
            AnimatorSet animationSet = new AnimatorSet();

            //Translating Details_Card in Y Scale
            ObjectAnimator card_y = ObjectAnimator.ofFloat(cardView, View.TRANSLATION_Y, 70);
            card_y.setDuration(2500);
            card_y.setRepeatMode(ValueAnimator.REVERSE);
            card_y.setRepeatCount(ValueAnimator.INFINITE);
            card_y.setInterpolator(new LinearInterpolator());

            //Translating Movie_Cover in Y Scale
            ObjectAnimator cover_y = ObjectAnimator.ofFloat(image, View.TRANSLATION_Y, 30);
            cover_y.setDuration(3000);
            cover_y.setRepeatMode(ValueAnimator.REVERSE);
            cover_y.setRepeatCount(ValueAnimator.INFINITE);
            cover_y.setInterpolator(new LinearInterpolator());

            animationSet.playTogether(card_y,cover_y);
            animationSet.start();




        }
        Bitmap bmp = null;
        String filename = getIntent().getStringExtra("bg");
        try {
            FileInputStream is = this.openFileInput(filename);
            bmp = BitmapFactory.decodeStream(is);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Picasso.with(this).load(intent.getStringExtra("bg")).into(image);

        image.setImageBitmap(bmp);
       // image.setImageResource(intent.getIntExtra("bg",1));
        //movie.setText(intent.getStringExtra("title"));


        details.setText(intent.getStringExtra("plot"));
       // Picasso.with(this).load(intent.getStringExtra("bg")).into(image);
    }
}
