package com.sport.grsport.Part4;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.sport.grsport.Adapters.ChildRowItems;
import com.sport.grsport.Adapters.HorizontalCustomAdapter;
import com.sport.grsport.Adapters.ProfileCustomAdapter;
import com.sport.grsport.HelperClasses.SharedPreference;
import com.sport.grsport.MainActivity;
import com.sport.grsport.Part3.Home;
import com.sport.grsport.R;
import com.sport.grsport.part1.userInformationClass;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import static com.sport.grsport.HelperClasses.Help.firebase;

public class MyCreateTeam extends AppCompatActivity implements  ProfileCustomAdapter.OnItemClick {

    int maximumplayer=18;
    FirebaseStorage storage;
    userInformationClass Person;
    String username;
    private FirebaseAuth auth;
    SharedPreference sharedPreference;
    List<ChildRowItems> rowItems;
    ListView listView;
    TextView textView_membercount;
    public StorageReference storageReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_create_team);
        sharedPreference=new SharedPreference(getApplicationContext());
        Firebase.setAndroidContext(this);
        Person=new userInformationClass();
        rowItems=new ArrayList<>();
        auth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();

        setText();
        SHAREDPREFERENCE();
        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }

        AsyncTaskRunner runner = new AsyncTaskRunner();

        runner.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void setText(){
        textView_membercount=(TextView)findViewById(R.id.createteamplayeremaining);
    }


    private void SHAREDPREFERENCE(){
        HashMap<String,String> user=sharedPreference.getUserDetails();
        username=user.get(sharedPreference.KEY_USERNAME);
        Person.setName(user.get(sharedPreference.KEY_NAME));
        Person.setAge(user.get(sharedPreference.KEY_AGE));
        Person.setEmail(user.get(sharedPreference.KEY_EMAIL));
        Person.setContactnumber(user.get(sharedPreference.KEY_PHONE));
        Person.setCity(user.get(sharedPreference.KEY_CITY));
        Person.setType(user.get(sharedPreference.KEY_TYPE));
        Person.setSchoolinstitudeinformation(sharedPreference.KEY_SchoolInformation);
        Person.setSchoolinstitudeinformation(user.get(sharedPreference.KEY_SchoolInformation));





    }

    public void countprofile(){


        firebase.child("userdetails").child(username).orderByChild("checkparent").equalTo("yes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot.getChildrenCount()+"");
                int noofchildpresent=(int) dataSnapshot.getChildrenCount();


                if(noofchildpresent>0){
                    maximumplayer=maximumplayer-noofchildpresent;
                    textView_membercount.setText(maximumplayer+" players remaining");
                    restofteamprofile();
                }else {
                    textView_membercount.setText(maximumplayer+" players remaining");
                }

                if(noofchildpresent==18){
                    ImageView linearLayout=(ImageView) findViewById(R.id.createteamaddmoreplayer);
                    linearLayout.setVisibility(View.GONE);
                    textView_membercount.setText("Bingo !! Team completed .");
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        }) ;

    }

    public void restofteamprofile(){
        // Firebase ref = new Firebase("https://showursport-b6e37.firebaseio.com/userdetails/"+susername);
        firebase.child("userdetails").child(username).orderByChild("checkparent").equalTo("yes").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ChildRowItems item = new ChildRowItems();

                String name=(String)dataSnapshot.child("name").getValue();
                String age=(String)dataSnapshot.child("age").getValue();
                String childid=(String) dataSnapshot.child("childid").getValue();

                String imagelinkcode=(String)dataSnapshot.child("image").getValue();

                item.setAge(age);
                item.setName(name);
                item.setImagelink(imagelinkcode);
                item.setChilduser(childid);

                rowItems.add(item);

                if(rowItems.size()>0){


                     LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(MyCreateTeam.this, LinearLayoutManager.VERTICAL, false);

                    RecyclerView   horizontal_recycler_view= (RecyclerView) findViewById(R.id.createteamrecycler);
                    ProfileCustomAdapter      horizontalAdapter=new ProfileCustomAdapter(getApplicationContext(),rowItems,MyCreateTeam.this);
                    horizontal_recycler_view.setHasFixedSize(true);

                    horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);

                    horizontal_recycler_view.setAdapter(horizontalAdapter);





                }else {

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }





    public void showremove(final String username){


        final CharSequence[] options = { "Remove from Team","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(MyCreateTeam.this);

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Remove from Team"))
                {
                    removefromteam(username);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    public void removefromteam(final String user){

        String susername=null;
        String imagelink=null;
        StringTokenizer stringTokenizer=new StringTokenizer(user,"$$$$$$$");
        while ((stringTokenizer.hasMoreTokens()))
        {
            susername=stringTokenizer.nextToken();

            imagelink=stringTokenizer.nextToken();

        }
        Log.d("susername",user);

        final String u=susername;
      //  Toast.makeText(getApplicationContext(),imagelink+"",Toast.LENGTH_LONG).show();
        final ProgressDialog mprogress1=new ProgressDialog(MyCreateTeam.this);
        mprogress1.setMessage("Loading..");
        mprogress1.setCancelable(false);
        mprogress1.setIndeterminate(false);
        mprogress1.show();



        if(!imagelink.equals("null")&& !susername.equals("null"))
        {

            firebase.child("userdetails").child(username).child(u).removeValue(new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError!=null){
                        Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();
                        mprogress1.dismiss();
                    }else {
                        Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT).show();
                        final   StorageReference riversRef = storageReference.child("profilepictures/").child(u+".jpg");

                        riversRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();
                            }
                        });

                        mprogress1.dismiss();
                        Intent intent = getIntent();
                        overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(intent);

                    }

                }
            });

        }else if(imagelink.equals("null")&& !susername.equals("null"))
        {
            firebase.child("userdetails").child(username).child(susername).removeValue(new Firebase.CompletionListener() {
                @Override
                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    if(firebaseError!=null){
                        Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();
                        mprogress1.dismiss();
                    }else {
                        Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT).show();


                        mprogress1.dismiss();
                        Intent intent = getIntent();
                        overridePendingTransition(0, 0);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(intent);
                    }

                }
            });
        }



    }

    public void onclickCreateteam(View view){
        Random r = new Random();
        int i1 = r.nextInt(8000099 - 6503) + 65;

        Intent intent=new Intent(getApplicationContext(),CreateUserProfile.class);
        Calendar c=Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String workoutstarttime = df.format(c.getTime());
        intent.putExtra("childuserName",username+workoutstarttime+i1);
        startActivity(intent);

        finish();
    }

    @Override
    public void onClick(String value) {

        Log.d("fdadffs",value+"");
        showremove(value);
    }


    private class AsyncTaskRunner extends AsyncTask<String, String, String> {


        String count;

        @Override
        protected String doInBackground(String... params) {

            countprofile();


            return count;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation






        }


        @Override
        protected void onPreExecute() {

        }


        @Override
        protected void onProgressUpdate(String... text) {


        }
    }

    public void onclickmycreateteamback(View view)
    {
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }
    @Override
    public void onBackPressed () {
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }

}
