package com.sport.grsport.Part4;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sport.grsport.HelperClasses.SharedPreference;
import com.sport.grsport.R;
import com.sport.grsport.part1.userInformationClass;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.sport.grsport.HelperClasses.Help.firebase;

public class UpdateChildUser extends AppCompatActivity {
    Uri downloadUrl;
    private static final int PICK_IMAGE_REQUEST = 234;
    private StorageReference storageRef;
    FirebaseStorage storage;
    private static final String TAG = "Storage#MainActivity";
    Bitmap bitmap;
    private Uri filePath;
    SharedPreference sharedPreference;
    String username,childusername;
    private StorageReference storageReference;
    ProgressDialog mprogress;
    userInformationClass Person;
    private FirebaseAuth auth;

    EditText editText_name,editText_age;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_child_user);
        auth = FirebaseAuth.getInstance();

        sharedPreference=new SharedPreference(getApplicationContext());
        HashMap<String,String> user=sharedPreference.getUserDetails();
        username=user.get(sharedPreference.KEY_USERNAME);

        storage = FirebaseStorage.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        SETTEXT();

        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.statuscolor));
        }

        Bundle bundle=getIntent().getExtras();
        if(bundle==null)
            return;

        childusername=bundle.getString("childuserName");


        //childusername=username+workoutstarttime;

    }

    public void SETTEXT(){
        editText_name=(EditText)findViewById(R.id.createupdateuser_name);
        editText_age=(EditText)findViewById(R.id.createupdateuser_age);
        imageView=(ImageView) findViewById(R.id.updateuser_profile_picture);

    }





    public void creteupdateuser_btn_save_data(View view){


        String  name=editText_name.getText().toString();
        String  age=editText_age.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(getApplicationContext(), "Enter name!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(age)) {
            Toast.makeText(getApplicationContext(), "Enter Age!", Toast.LENGTH_SHORT).show();
            return;
        }




        mprogress=new ProgressDialog(UpdateChildUser.this);
        mprogress.setMessage("Loading..");
        mprogress.setIndeterminate(false);
        mprogress.setCancelable(false);
        mprogress.show();
        //Storing values to firebase

        Firebase usersRef  = firebase.child("userdetails").child(username).child(childusername);
        Map<String,Object> nickname=new HashMap<String,Object>();

        nickname.put("name",name);
        nickname.put("age",age);
        nickname.put("checkparent","yes");
        nickname.put("childid",childusername);



        usersRef.updateChildren(nickname, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if (firebaseError != null) {
                    Toast.makeText(UpdateChildUser.this, "error", Toast.LENGTH_LONG).show();
                    mprogress.dismiss();
                } else {
                    // Profileimagesession.createprofileSession(stringUri);
                    Toast.makeText(UpdateChildUser.this, "done", Toast.LENGTH_LONG).show();


                    mprogress.dismiss();
                    startActivity(new Intent(getApplicationContext(),Profile.class));
                    finish();

                }

            }
        });



    }




    @Override
    public void onBackPressed () {
        Intent setIntent = new Intent(this, Profile.class);
        startActivity(setIntent);
        finish();
    }

    public void onclickcreateuserpreviousactivity(View view){
        Intent setIntent = new Intent(this, Profile.class);
        startActivity(setIntent);
        finish();
    }

    public void onclickupdateuserprofilepicture(View view){
        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(UpdateChildUser.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), childusername+".jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                }
                else if (options[item].equals("Choose from Gallery"))
                {

                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);


                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals(childusername + ".jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {

                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            bitmapOptions);
                    storageRef = storage.getReferenceFromUrl("gs://grsport-69208.appspot.com");
                    bitmap = Bitmap.createScaledBitmap(bitmap, 1280, 1280, true);
                    imageView.setImageBitmap(bitmap);

                    StorageReference mountainsRef = storageRef.child(childusername + ".jpg");


                    StorageReference mountainImagesRef = storageRef.child("images/mountains.jpg");


                    mountainsRef.getName().equals(mountainImagesRef.getName());    // true
                    mountainsRef.getPath().equals(mountainImagesRef.getPath());    // false


                    imageView.setDrawingCacheEnabled(true);
                    imageView.buildDrawingCache();
                    bitmap = imageView.getDrawingCache();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data1 = baos.toByteArray();



                    UploadTask uploadTask = mountainsRef.putBytes(data1);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {

                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                            downloadUrl = taskSnapshot.getDownloadUrl();
                            Toast.makeText(UpdateChildUser.this, "completed", Toast.LENGTH_LONG).show();
                            updatedetails();
                        }
                    });


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
                filePath = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    imageView.setImageBitmap(bitmap);
                    uploadFile();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        }
    }

    private void uploadFile() {
        //if there is a file to upload
        if (filePath != null) {
            //displaying a progress dialog while upload is going on
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StorageReference riversRef = storageReference.child(childusername+".jpg");
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //if the upload is successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();
                            downloadUrl = taskSnapshot.getDownloadUrl();
                            //  Toast.makeText(EnterInformation.this, "completed"+downloadUrl, Toast.LENGTH_LONG).show();
                            updatedetails();
                            //and displaying a success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();

                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                            //displaying percentage in progress dialog
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        }
        //if there is not any file
        else {
            //you can display an error toast
        }
    }





    public void updatedetails(){
        // Firebase myFirebaseRef = new Firebase("https://showursport-b6e37.firebaseio.com/userdetails");
        Firebase usersRef  = firebase.child("userdetails").child(username).child(childusername);
        Map<String,Object> nickname=new HashMap<String,Object>();

        final String stringUri;
        stringUri = downloadUrl.toString();
        nickname.put("image",stringUri);

        usersRef.updateChildren(nickname, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if (firebaseError != null) {
                    //mprogress1.dismiss();
                    Toast.makeText(UpdateChildUser.this, "error", Toast.LENGTH_LONG).show();
                } else {
                    // mprogress1.dismiss();
                    // Profileimagesession.createprofileSession(stringUri);
                    Toast.makeText(UpdateChildUser.this, "done", Toast.LENGTH_LONG).show();



                }
            }
        });
    }

}