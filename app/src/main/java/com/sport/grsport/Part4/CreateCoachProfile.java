package com.sport.grsport.Part4;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sport.grsport.HelperClasses.SharedPreference;
import com.sport.grsport.R;
import com.sport.grsport.part1.userInformationClass;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.sport.grsport.HelperClasses.Help.firebase;

public class CreateCoachProfile extends AppCompatActivity {
    Uri downloadUrl;
    private Uri mCropImageUri;
    private static final int PICK_IMAGE_REQUEST = 234;
    private StorageReference storageRef;
    FirebaseStorage storage;
    private static final String TAG = "Storage#MainActivity";
    Bitmap bitmap;
    private Uri filePath;
    SharedPreference sharedPreference;
    String username,childusername;
    private StorageReference storageReference;
    ProgressDialog mprogress;
    userInformationClass Person;
    private FirebaseAuth auth;

    EditText editText_name,editText_age,editText_description;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_coach_profile);
        auth = FirebaseAuth.getInstance();

        sharedPreference=new SharedPreference(getApplicationContext());
        HashMap<String,String> user=sharedPreference.getUserDetails();
        username=user.get(sharedPreference.KEY_USERNAME);

        storage = FirebaseStorage.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        SETTEXT();

        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }

        Bundle bundle=getIntent().getExtras();
        if(bundle==null)
            return;

        childusername=bundle.getString("childCoachName")+"coach";


        //childusername=username+workoutstarttime;

    }

    public void SETTEXT(){
        editText_name=(EditText)findViewById(R.id.createcoach_name);
        editText_age=(EditText)findViewById(R.id.createcoach_age);
        imageView=(ImageView) findViewById(R.id.createcoach_profile_picture);
        editText_description=(EditText)findViewById(R.id.createcoach_description);

    }



    @Override
    public void onBackPressed () {
        Intent setIntent = new Intent(this, Profile.class);
        startActivity(setIntent);
        finish();
    }

    public void onclickcoachpreviousactivity(View view){
        Intent setIntent = new Intent(this, Profile.class);
        startActivity(setIntent);
        finish();
    }

    public void creteuser_btn_save_data(View view){


        String  name=editText_name.getText().toString();
        String  age=editText_age.getText().toString();
        String  description=editText_description.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(getApplicationContext(), "Enter name!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(age)) {
            Toast.makeText(getApplicationContext(), "Enter Age!", Toast.LENGTH_SHORT).show();
            return;
        }


        if(Integer.parseInt(age)<5 || Integer.parseInt(age)>85)
        {
            Toast.makeText(getApplicationContext(), "Enter Valid Age!", Toast.LENGTH_SHORT).show();
            return;
        }


        mprogress=new ProgressDialog(CreateCoachProfile.this);
        mprogress.setMessage("Loading..");
        mprogress.setIndeterminate(false);
        mprogress.setCancelable(false);
        mprogress.show();
        //Storing values to firebase

        Firebase usersRef  = firebase.child("userdetails").child(username).child(childusername);
        Map<String,Object> nickname=new HashMap<String,Object>();

        nickname.put("name",name);
        nickname.put("age",age);
        nickname.put("description",description);
        nickname.put("coachid",childusername);
        nickname.put("coachpresent","yes");



        usersRef.updateChildren(nickname, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if (firebaseError != null) {
                    Toast.makeText(CreateCoachProfile.this, "error", Toast.LENGTH_LONG).show();
                    mprogress.dismiss();
                } else {
                    // Profileimagesession.createprofileSession(stringUri);
                    Toast.makeText(CreateCoachProfile.this, "done", Toast.LENGTH_LONG).show();


                    mprogress.dismiss();
                    startActivity(new Intent(getApplicationContext(),Profile.class));
                    finish();

                }

            }
        });



    }





    public void onclickcreatecoachprofilepicture(View view){
        CropImage.startPickImageActivity(this);
    }
    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageView.setImageURI(result.getUri());
                uploadFile(result.getUri());
               // Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAspectRatio(512,512)
                .setFixAspectRatio(true)
                .start(this);
    }

    private void uploadFile(Uri filePath) {
        //if there is a file to upload
        if (filePath != null) {
            //displaying a progress dialog while upload is going on
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StorageReference riversRef = storageReference.child("profilepictures/").child(childusername+".jpg");
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //if the upload is successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();
                            downloadUrl = taskSnapshot.getDownloadUrl();
                            //  Toast.makeText(EnterInformation.this, "completed"+downloadUrl, Toast.LENGTH_LONG).show();
                            updatedetails();
                            //and displaying a success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();

                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                            //displaying percentage in progress dialog
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        }
        //if there is not any file
        else {
            Toast.makeText(getApplicationContext(), "Error !! please try again later ", Toast.LENGTH_LONG).show();
        }
    }






    public void updatedetails(){
        // Firebase myFirebaseRef = new Firebase("https://showursport-b6e37.firebaseio.com/userdetails");
        Firebase usersRef  = firebase.child("userdetails").child(username).child(childusername);
        Map<String,Object> nickname=new HashMap<String,Object>();

        final String stringUri;
        stringUri = downloadUrl.toString();
        nickname.put("image",stringUri);

        usersRef.updateChildren(nickname, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if (firebaseError != null) {
                    //mprogress1.dismiss();
                    Toast.makeText(CreateCoachProfile.this, "error", Toast.LENGTH_LONG).show();
                } else {
                    // mprogress1.dismiss();
                    // Profileimagesession.createprofileSession(stringUri);
                    Toast.makeText(CreateCoachProfile.this, "done", Toast.LENGTH_LONG).show();



                }
            }
        });
    }

}
