package com.sport.grsport.Part4;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sport.grsport.Adapters.ChildRowItems;
import com.sport.grsport.Adapters.ProfileCustomAdapter;
import com.sport.grsport.Adapters.ProfileRowItems;
import com.sport.grsport.HelperClasses.CircleTransform;
import com.sport.grsport.HelperClasses.Help;
import com.sport.grsport.HelperClasses.SharedPreference;
import com.sport.grsport.Part2.EnterInformation;
import com.sport.grsport.Part3.Home;
import com.sport.grsport.R;
import com.sport.grsport.part1.login;
import com.sport.grsport.part1.userInformationClass;
import com.squareup.picasso.Cache;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.StringTokenizer;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import static com.sport.grsport.HelperClasses.Help.firebase;
import static com.sport.grsport.HelperClasses.Help.isNetworkStatusAvialable;

public class Profile extends AppCompatActivity  implements AdapterView.OnItemClickListener{

    private Uri mCropImageUri;
    Uri downloadUrl;
    private static final int PICK_IMAGE_REQUEST = 234;
    private StorageReference storageRef;
    private Uri filePath;
    Bitmap bitmap;
    String imageurl;
    FirebaseStorage storage;
    SharedPreference sharedPreference;
    String username;
    CardView cardView_coach;
    ProgressDialog mprogress;
    LinearLayout hidemultiprofilelinearlayout,countlinearlayout;
    userInformationClass Person;
    private FirebaseAuth auth;
    Dialog dialog1;
    int maximumplayer=18;
    private StorageReference storageReference;
    TextView textView_name,textView_age,textView_email,textView_contact,textView_city,textView_membercount,textView_type,textView_school;
    TextView textView_coachname,textView_coachage,textView_description;
    ImageView imageView,imageView_coach;

    String coachid,coachimagelink;
    ListView listView;
    List<ChildRowItems> rowItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Firebase.setAndroidContext(this);

        Person=new userInformationClass();
        auth = FirebaseAuth.getInstance();
        rowItems=new ArrayList<>();

        sharedPreference=new SharedPreference(getApplicationContext());

        storage = FirebaseStorage.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(getApplicationContext(),login.class));
            finish();
        }
        setTEXT();
        SHAREDPREFERENCE();
        setprofile();



        if(Build.VERSION.SDK_INT>=21){
            Window window=this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.appcolor));
        }
        imageView.setFocusableInTouchMode(true);
    }

    public void setprofile(){
        // Firebase ref = new Firebase("https://showursport-b6e37.firebaseio.com/userdetails/");
        firebase.child("userdetails").child(username).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                imageurl = (String) snapshot.child("image").getValue();
                // Toast.makeText(Home.this,value,Toast.LENGTH_SHORT).show();


                Picasso.with(Profile.this)
                        .load(imageurl)
                        .error(R.drawable.noprofilepicture)
                        .transform(new CircleTransform())
                        .into(imageView);

              /*  Picasso.with(Profile.this)
                        .load(value)
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                RoundedBitmapDrawable drawable = Help.createRoundedBitmapDrawableWithBorder(bitmap);


                                imageView.setImageDrawable(drawable);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });*/
            }
            @Override public void onCancelled(FirebaseError error) { }
        });

    }

    private void setTEXT(){
        textView_school=(TextView)findViewById(R.id.profile_school);
        cardView_coach=(CardView)findViewById(R.id.Cardview_coachprofile);
        textView_coachage=(TextView) findViewById(R.id.profile_coach_age);
        textView_coachname=(TextView) findViewById(R.id.profile_coach_name);
        textView_description=(TextView) findViewById(R.id.profile_coach_description);
        textView_membercount=(TextView)findViewById(R.id.profile_membercount);
        textView_name=(TextView)findViewById(R.id.profile_name);
        textView_age=(TextView)findViewById(R.id.profile_age);
        textView_email=(TextView)findViewById(R.id.profile_email);
        textView_contact=(TextView)findViewById(R.id.profile_mobilenumber);
        textView_city=(TextView)findViewById(R.id.profile_city);
        imageView=(ImageView) findViewById(R.id.profile_main_image);
        imageView_coach=(ImageView)findViewById(R.id.profile_coach_image);
        textView_type=(TextView)findViewById(R.id.profile_type);
        hidemultiprofilelinearlayout=(LinearLayout)findViewById(R.id.hideteamprofiledetails);
        countlinearlayout=(LinearLayout)findViewById(R.id.countlinearlayout);
    }

    private void SHAREDPREFERENCE(){
        HashMap<String,String> user=sharedPreference.getUserDetails();
        username=user.get(sharedPreference.KEY_USERNAME);
        Person.setName(user.get(sharedPreference.KEY_NAME));
       Person.setAge(user.get(sharedPreference.KEY_AGE));
        Person.setEmail(user.get(sharedPreference.KEY_EMAIL));
       Person.setContactnumber(user.get(sharedPreference.KEY_PHONE));
        Person.setCity(user.get(sharedPreference.KEY_CITY));
        Person.setType(user.get(sharedPreference.KEY_TYPE));
        Person.setSchoolinstitudeinformation(sharedPreference.KEY_SchoolInformation);
        Person.setSchoolinstitudeinformation(user.get(sharedPreference.KEY_SchoolInformation));

        textView_age.setText(Person.getAge());
        textView_email.setText(Person.getEmail());
        textView_contact.setText(Person.getContactnumber());
        textView_name.setText(Person.getName());
        textView_city.setText(Person.getCity());
        textView_school.setText(Person.getSchoolinstitudeinformation());

        if(Person.getType().equals("Single Registration")){
            countlinearlayout.setVisibility(View.GONE);
            hidemultiprofilelinearlayout.setVisibility(View.GONE);
            textView_type.setText("Single");

        }else if(Person.getType().equals("Team Registration")){
            countlinearlayout.setVisibility(View.VISIBLE);
            textView_type.setText("Team");
            countcoach();
            countprofile();
        }

    }



    public void onclickEditProfile(View view){

        if(imageView.getDrawable()!=null) {
            final CharSequence[] options = {"Edit Profile", "Remove Profile Picture", "Cancel"};

            AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);
            builder.setTitle("Edit Profile !!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Edit Profile")) {
                        EditProfileDialog();
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    } else if (options[item].equals("Remove Profile Picture")) {
                        StorageReference riversRef = storageReference.child("profilepictures/").child(username + ".jpg");


                        riversRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                imageView.setImageBitmap(null);
                                firebase.child("userdetails").child(username).child("image").removeValue(new Firebase.CompletionListener() {
                                    @Override
                                    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                        if (firebaseError != null) {
                                            Toast.makeText(getApplicationContext(), "Error !! try again later", Toast.LENGTH_SHORT).show();

                                        } else {
                                            Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Uh-oh, an error occurred!
                            }
                        });
                    }
                }
            });
            builder.show();

        }else if(imageView.getDrawable()==null){
            EditProfileDialog();
        }

    }

    private void EditProfileDialog()

    {
        if (isNetworkStatusAvialable(getApplicationContext())) {
            dialog1 = new Dialog(Profile.this);
            dialog1.setContentView(R.layout.dialog_edit_user_profile);
            dialog1.setCancelable(false);
            dialog1.show();
            final EditText editText_name = (EditText) dialog1.findViewById(R.id.dialog_name);
            final EditText editText_age = (EditText) dialog1.findViewById(R.id.dialog_age);
            final EditText editText_contact = (EditText) dialog1.findViewById(R.id.dialog_contact_number);
            final EditText editText_city = (EditText) dialog1.findViewById(R.id.dialog_city);

            RelativeLayout ok = (RelativeLayout) dialog1.findViewById(R.id.dialog_saveok);
            RelativeLayout cancel = (RelativeLayout) dialog1.findViewById(R.id.dialog_savecancel);


            editText_age.setText(Person.getAge());
            editText_name.setText(Person.getName());
            editText_city.setText(Person.getCity());
            editText_contact.setText(Person.getContactnumber());

            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Person.setName(editText_name.getText().toString());
                    Person.setAge(editText_age.getText().toString());
                    Person.setCity(editText_city.getText().toString());
                    Person.setContactnumber(editText_contact.getText().toString());

                    if (TextUtils.isEmpty(Person.getName())) {
                        Toast.makeText(getApplicationContext(), "Enter name !", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(Person.getAge())) {
                        Toast.makeText(getApplicationContext(), "Enter age!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(Person.getCity())) {
                        Toast.makeText(getApplicationContext(), "Enter city!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(Person.getContactnumber())) {
                        Toast.makeText(getApplicationContext(), "Enter contact number!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(Integer.parseInt(Person.getAge())<5 || Integer.parseInt(Person.getAge())>85)
                    {
                        Toast.makeText(getApplicationContext(), "Enter Valid Age!", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    Insterdata(Person);

                }
            });

            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog1.dismiss();
                }
            });
        }else {
            Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
        }
    }

    public void Insterdata(final userInformationClass person){

        mprogress=new ProgressDialog(Profile.this);
        mprogress.setMessage("Loading..");
        mprogress.setCancelable(false);
        mprogress.setIndeterminate(false);
        mprogress.show();

        Firebase usersRef  = firebase.child("userdetails").child(username);
        Map<String,Object> nickname=new HashMap<String,Object>();

        nickname.put("username",username);
        nickname.put("name",person.getName());
        nickname.put("age",person.getAge());
        nickname.put("city",person.getCity());
        nickname.put("email",person.getEmail());
        nickname.put("contactnumber",person.getContactnumber());


        usersRef.updateChildren(nickname, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if (firebaseError != null) {
                    Toast.makeText(Profile.this, "error", Toast.LENGTH_LONG).show();
                    mprogress.dismiss();
                } else {
                    // Profileimagesession.createprofileSession(stringUri);
                    //Toast.makeText(Profile.this, "done", Toast.LENGTH_LONG).show();
                    sharedPreference.createDetailsSession(username,person.getName(),person.getAge(),person.getContactnumber(),person.getCity(),person.getEmail(),person.getSchoolinstitudeinformation(),person.getType());
                    dialog1.dismiss();
                    mprogress.dismiss();
                    Intent intent = getIntent();
                    overridePendingTransition(0, 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onBackPressed () {
        Intent setIntent = new Intent(this, Home.class);
        startActivity(setIntent);
        finish();
    }


    public void countcoach(){

       final TextView  linearLayout=(TextView) findViewById(R.id.hideaddcoach);
        final TextView  coachcount=(TextView) findViewById(R.id.profile_coachcount);
        firebase.child("userdetails").child(username).orderByChild("coachpresent").equalTo("yes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot.getChildrenCount()+"");
                int noofchildpresent=(int) dataSnapshot.getChildrenCount();


                if(noofchildpresent>0){

                    linearLayout.setFocusableInTouchMode(false);
                    linearLayout.setVisibility(View.GONE);
                    restofcoach();
                    coachcount.setText(noofchildpresent+"");
                }else {

                    coachcount.setText("0");
                    linearLayout.setVisibility(View.VISIBLE);
                    cardView_coach.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        }) ;


    }

    private void restofcoach(){
        final ProgressBar progressBar=(ProgressBar)findViewById(R.id.progressbarcoach);
        progressBar.setVisibility(View.VISIBLE);
        firebase.child("userdetails").child(username).orderByChild("coachpresent").equalTo("yes").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {


                progressBar.setVisibility(View.GONE);
                String name=(String)dataSnapshot.child("name").getValue();
                String age=(String)dataSnapshot.child("age").getValue();
                 coachid=(String) dataSnapshot.child("coachid").getValue();
                String description=(String) dataSnapshot.child("description").getValue();
                coachimagelink=(String)dataSnapshot.child("image").getValue();
                textView_coachage.setText(age);
                textView_coachname.setText(name);
                textView_description.setText(description);

                Picasso.with(Profile.this)
                        .load(coachimagelink)
                        .error(R.drawable.noprofilepicture)
                        .transform(new CircleTransform())
                        .into(imageView_coach);


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    public void countprofile(){


        firebase.child("userdetails").child(username).orderByChild("checkparent").equalTo("yes").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot.getChildrenCount()+"");
                int noofchildpresent=(int) dataSnapshot.getChildrenCount();


                if(noofchildpresent>0){
                    //maximumplayer=maximumplayer-noofchildpresent;
                    textView_membercount.setText(noofchildpresent+"");

                }else {
                    textView_membercount.setText(noofchildpresent+"");
                }


            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        }) ;

    }

    ///////////////////
    //////end number of profile
    /////////////////


    ///////////////////
    //////sub profile
    /////////////////
   /* public void restofteamprofile(){
       // Firebase ref = new Firebase("https://showursport-b6e37.firebaseio.com/userdetails/"+susername);
        firebase.child("userdetails").child(username).orderByChild("checkparent").equalTo("yes").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ChildRowItems item = new ChildRowItems();

                String name=(String)dataSnapshot.child("name").getValue();
                String age=(String)dataSnapshot.child("age").getValue();
                String childid=(String) dataSnapshot.child("childid").getValue();

                String imagelinkcode=(String)dataSnapshot.child("image").getValue();

              item.setAge(age);
                item.setName(name);
                item.setImagelink(imagelinkcode);
                item.setChilduser(childid);

                rowItems.add(item);

                if(rowItems.size()>0){
                    listView = (ListView) findViewById(R.id.profile_listview_member);
                    ProfileCustomAdapter adapter = new ProfileCustomAdapter(getApplicationContext(), rowItems);
                    //listView.setAdapter(adapter);
                    ListUtils.setDynamicHeight(listView);
                    listView.setOnItemClickListener(Profile.this);
                    listView.setFocusableInTouchMode(false);

                }else {

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }*/


    ///////////////////
    //////end sub profile
    /////////////////




    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
       Object obj = listView.getAdapter().getItem(position);
        String username=obj.toString();
       // Toast.makeText(getApplicationContext(),username,Toast.LENGTH_LONG).show();
       // showremove(username);
        //toast = Toast.makeText(getApplicationContext(), "Item " + (position + 1) + ": " + rowItems.get(position),
        //      Toast.LENGTH_SHORT);




    }


   /* public void showremove(final String username){


        final CharSequence[] options = { "Remove from Team","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
              if (options[item].equals("Remove from Team"))
                {
                    removefromteam(username);
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }*/



    public void OnclickRemovecoach(View view){

        final CharSequence[] options = { "Remove Coach","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);

        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Remove Coach"))
                {
                    RemoveCoach();
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();



    }


    private void RemoveCoach(){

        if (isNetworkStatusAvialable(getApplicationContext())) {


            final ProgressDialog mprogress1 = new ProgressDialog(Profile.this);
            mprogress1.setMessage("Loading..");
            mprogress1.setIndeterminate(false);
            mprogress1.setCancelable(false);
            mprogress1.show();


            if(coachimagelink!=null&& coachid!=null)
            {

                firebase.child("userdetails").child(username).child(coachid).removeValue(new Firebase.CompletionListener() {
                    @Override
                    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                        if(firebaseError!=null){
                            Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();
                            mprogress1.dismiss();
                        }else {
                            Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT).show();
                            final   StorageReference riversRef = storageReference.child("profilepictures/").child(coachid+".jpg");

                            riversRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();
                                }
                            });

                            mprogress1.dismiss();
                            Intent intent = getIntent();
                            overridePendingTransition(0, 0);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(intent);

                        }

                    }
                });

            }else if(coachimagelink==null&& coachid!=null)
            {
                firebase.child("userdetails").child(username).child(coachid).removeValue(new Firebase.CompletionListener() {
                    @Override
                    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                        if(firebaseError!=null){
                            Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();
                            mprogress1.dismiss();
                        }else {
                            Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT).show();


                            mprogress1.dismiss();
                            Intent intent = getIntent();
                            overridePendingTransition(0, 0);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                        }

                    }
                });
            }



           /* StorageReference riversRef = storageReference.child(coachid + ".jpg");


            riversRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    firebase.child("userdetails").child(username).child(coachid).removeValue(new Firebase.CompletionListener() {
                        @Override
                        public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                            if (firebaseError != null) {


                                Toast.makeText(getApplicationContext(), "Error !! try again later", Toast.LENGTH_SHORT).show();
                                mprogress1.dismiss();
                            } else {

                                Toast.makeText(getApplicationContext(), "Done", Toast.LENGTH_SHORT).show();
                                mprogress1.dismiss();
                                Intent intent = getIntent();
                                overridePendingTransition(0, 0);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                                finish();
                                overridePendingTransition(0, 0);
                                startActivity(intent);
                            }

                        }
                    });

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(getApplicationContext(), "Error !! try again later", Toast.LENGTH_SHORT).show();
                }
            });*/

        }else {
            Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
        }
    }

   /* public void removefromteam(final String user){

        String susername=null;
        String imagelink=null;
        StringTokenizer stringTokenizer=new StringTokenizer(user,"%&%&%&%&");
        while ((stringTokenizer.hasMoreTokens()))
        {
            susername=stringTokenizer.nextToken();
            Log.d("checcckckck",susername);
            imagelink=stringTokenizer.nextToken();

        }

        Toast.makeText(getApplicationContext(),imagelink+"",Toast.LENGTH_LONG).show();
      final ProgressDialog  mprogress1=new ProgressDialog(Profile.this);
        mprogress1.setMessage("Loading..");
        mprogress1.setCancelable(false);
        mprogress1.setIndeterminate(false);
        mprogress1.show();



        if(!imagelink.equals("null"))
        {
            final   StorageReference riversRef = storageReference.child(user+".jpg");

            riversRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();
                }
            });
        }

        firebase.child("userdetails").child(username).child(susername).removeValue(new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if(firebaseError!=null){
                    Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();
                    mprogress1.dismiss();
                }else {
                    Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT).show();

                    mprogress1.dismiss();
                    Intent intent = getIntent();
                    overridePendingTransition(0, 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(intent);

                }

            }
        });








    }*/

    //////
    ////
    //////

    /*public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }*/








    ///////
    /////  create team activity
    ///////////


    ///////
    ///// end create team activity
    ///////////


    public void onclickprofilepreviousactivity(View view){
        startActivity(new Intent(getApplicationContext(),Home.class));
        finish();
    }



    public void onclickCreatecoach(View view)
    {
        Random r = new Random();
        int i1 = r.nextInt(80000 - 6503) + 65;

        Intent intent=new Intent(getApplicationContext(),CreateCoachProfile.class);
        Calendar c=Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String workoutstarttime = df.format(c.getTime());
        intent.putExtra("childCoachName",username+"coachid"+workoutstarttime+i1);
        startActivity(intent);

        finish();
    }



















    //////
    ////
    ////////




    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageView.setImageURI(result.getUri());
                uploadFile(result.getUri());
                //Toast.makeText(this, "Cropping successful " , Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // required permissions granted, start crop image activity
            startCropImageActivity(mCropImageUri);
        } else {
            Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMultiTouchEnabled(true)
                .setAspectRatio(512,512)
                .setFixAspectRatio(true)
                .start(this);
    }





    //////
    // image upload
    ////
    public void onclickchnageprofilepicture(View view){

        if (isNetworkStatusAvialable(getApplicationContext())) {

            if (imageView.getDrawable() == null) {


                CropImage.startPickImageActivity(this);
           /* final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };

            AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo"))
                    {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), username+".jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, 1);
                    }
                    else if (options[item].equals("Choose from Gallery"))
                    {

                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);


                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
            builder.show();*/
            } else if (imageView.getDrawable() != null) {
                CropImage.startPickImageActivity(this);
            /*final CharSequence[] options = { "Take Photo", "Choose from Gallery","Remove Profile Picture","Cancel" };

            AlertDialog.Builder builder = new AlertDialog.Builder(Profile.this);
            builder.setTitle("Add Photo!");
            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo"))
                    {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File f = new File(android.os.Environment.getExternalStorageDirectory(), username+".jpg");
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                        startActivityForResult(intent, 1);
                    }
                    else if (options[item].equals("Choose from Gallery"))
                    {

                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);


                    }
                    else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                    else if (options[item].equals("Remove Profile Picture")) {
                        StorageReference riversRef = storageReference.child(username+".jpg");


                        riversRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                imageView.setImageBitmap(null);
                                firebase.child("userdetails").child(username).child("image").removeValue(new Firebase.CompletionListener() {
                                    @Override
                                    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                        if(firebaseError!=null){
                                            Toast.makeText(getApplicationContext(),"Error !! try again later",Toast.LENGTH_SHORT).show();

                                        }else {
                                            Toast.makeText(getApplicationContext(),"Done",Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                });

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Uh-oh, an error occurred!
                            }
                        });
                    }
                }
            });
            builder.show();*/
            }
        }else {
            Toast.makeText(getApplicationContext(),"No Internet Connection !!",Toast.LENGTH_SHORT).show();
        }
    }




    private void uploadFile(Uri filePath) {
        //if there is a file to upload
        if (filePath != null) {
            //displaying a progress dialog while upload is going on
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading");
            progressDialog.setCancelable(false);
            progressDialog.show();

            StorageReference riversRef = storageReference.child("profilepictures/").child(username+".jpg");
            riversRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //if the upload is successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();
                            downloadUrl = taskSnapshot.getDownloadUrl();
                            //  Toast.makeText(EnterInformation.this, "completed"+downloadUrl, Toast.LENGTH_LONG).show();
                            updatedetails();
                            //and displaying a success toast
                            Toast.makeText(getApplicationContext(), "File Uploaded ", Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            //if the upload is not successfull
                            //hiding the progress dialog
                            progressDialog.dismiss();

                            //and displaying error message
                            Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //calculating progress percentage
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                            //displaying percentage in progress dialog
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        }
        //if there is not any file
        else {
            Toast.makeText(getApplicationContext(), "Error !! please try again later ", Toast.LENGTH_LONG).show();
        }
    }





    public void updatedetails(){
        // Firebase myFirebaseRef = new Firebase("https://showursport-b6e37.firebaseio.com/userdetails");
        Firebase usersRef  = firebase.child("userdetails").child(username);
        Map<String,Object> nickname=new HashMap<String,Object>();

        final String stringUri;
        stringUri = downloadUrl.toString();
        nickname.put("image",stringUri);

        usersRef.updateChildren(nickname, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if (firebaseError != null) {
                    //mprogress1.dismiss();
                    Toast.makeText(Profile.this, "error", Toast.LENGTH_LONG).show();
                } else {
                    // mprogress1.dismiss();
                    // Profileimagesession.createprofileSession(stringUri);
                    Toast.makeText(Profile.this, "done", Toast.LENGTH_LONG).show();
                    setprofile();
                    /*Intent intent = getIntent();
                    overridePendingTransition(0, 0);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(intent);*/


                }
            }
        });
    }





}
